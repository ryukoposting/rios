#ifndef __KERNEL_ARCH_I686_INTERRUPTS_H__
#define __KERNEL_ARCH_I686_INTERRUPTS_H__

#include <kernel.h>

/* same as SerenityOS register dump */
typedef struct CoreDump {
  uint16_t ss;
  uint16_t gs;
  uint16_t fs;
  uint16_t es;
  uint16_t ds;
  uint32_t edi;
  uint32_t esi;
  uint32_t ebp;
  uint32_t esp;
  uint32_t ebx;
  uint32_t edx;
  uint32_t ecx;
  uint32_t eax;
  uint16_t exception_code;
  uint16_t _padding_exception_code;
  uint32_t eip;
  uint16_t cs;
  uint16_t _padding_cs;
  uint32_t eflags;
  uint32_t esp_crossring;
  uint16_t ss_crossring;
} __attribute__((packed)) CoreDump;

/* SS, ESP, EFLAGS, CS, EIP, and error code are
 already on the stack when this code is touched.
 on entry to this, ESP points to error code*/
#define EXCEPTION_ENTRY(exn) \
  void exception_##exn##_handler(CoreDump cd); \
  extern void exception_##exn##_entry(); \
  asm ( \
    ".globl exception_" #exn "_entry\n" \
    "exception_" #exn "_entry: \n" \
    "    pushl $0x0\n" \
    "    pusha\n" \
    "    pushw %ds\n" \
    "    pushw %es\n" \
    "    pushw %fs\n" \
    "    pushw %gs\n" \
    "    pushw %ss\n" \
    "    pushw %ss\n" \
    "    pushw %ss\n" \
    "    pushw %ss\n" \
    "    pushw %ss\n" \
    "    popw %ds\n" \
    "    popw %es\n" \
    "    popw %fs\n" \
    "    popw %gs\n" \
    "    mov %esp, %eax\n" \
    "    call exception_" #exn "_handler\n" \
    "    popw %gs\n" \
    "    popw %gs\n" \
    "    popw %fs\n" \
    "    popw %es\n" \
    "    popw %ds\n" \
    "    popa\n" \
    "    add $0x4, %esp\n" \
    "    iret\n");

#define CONTEXT_SWITCH_ENTRY(exn) \
  Process *exception_##exn##_handler(CoreDump cd); \
  extern void exception_##exn##_entry(); \
  asm ( \
    ".globl exception_" #exn "_entry\n" \
    "exception_" #exn "_entry: \n" \
    "    pushl $0x0\n" \
    "    pusha\n" \
    "    pushw %ds\n" \
    "    pushw %es\n" \
    "    pushw %fs\n" \
    "    pushw %gs\n" \
    "    pushw %ss\n" \
    "    pushw %ss\n" \
    "    pushw %ss\n" \
    "    pushw %ss\n" \
    "    pushw %ss\n" \
    "    popw %ds\n" \
    "    popw %es\n" \
    "    popw %fs\n" \
    "    popw %gs\n" \
    "    mov %esp, %eax\n" \
    "    call exception_" #exn "_handler\n" \
    "    test %eax, %eax\n" \
    "    jz no_switch\n" \
    "no_switch:\n" \
    "    popw %gs\n" \
    "    popw %gs\n" \
    "    popw %fs\n" \
    "    popw %es\n" \
    "    popw %ds\n" \
    "    popa\n" \
    "    add $0x4, %esp\n" \
    "    iret\n");

#define KERNEL_FAULT_EXCEPTION(exn, msg) \
    void exception_##exn##_handler(CoreDump cd) \
    { \
      kernel_fault_errcode(msg, cd.exception_code);\
    } \
  extern void kernel_fault_##exn(); \
  asm ( \
    ".globl kernel_fault_" #exn "\n" \
    "kernel_fault_" #exn ": \n" \
    "    pushl $0x0\n" \
    "    pusha\n" \
    "    pushw %ds\n" \
    "    pushw %es\n" \
    "    pushw %fs\n" \
    "    pushw %gs\n" \
    "    pushw %ss\n" \
    "    pushw %ss\n" \
    "    pushw %ss\n" \
    "    pushw %ss\n" \
    "    pushw %ss\n" \
    "    popw %ds\n" \
    "    popw %es\n" \
    "    popw %fs\n" \
    "    popw %gs\n" \
    "    mov %esp, %eax\n" \
    "    call exception_" #exn "_handler\n" \
    "    popw %gs\n" \
    "    popw %gs\n" \
    "    popw %fs\n" \
    "    popw %es\n" \
    "    popw %ds\n" \
    "    popa\n" \
    "    add $0x4, %esp\n" \
    "    iret\n");

#endif
