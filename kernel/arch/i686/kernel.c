#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include <kernel.h>
#include <multiboot.h>
#include <scheduler.h>
#include <vmem.h>
#include <page.h>
#include <context.h>
#include <keyboard.h>
#include <arch/i686/arch_context.h>
#include <sections.h>
#include "linker.h"
#include "pic.h"

/* Check if the compiler thinks you are targeting the wrong operating system. */
#if defined(__linux__)
#error
#endif

/* This tutorial will only work for the 32-bit ix86 targets. */
#if !defined(__i386__)
#error
#endif

const size_t VGA_WIDTH = 80;
const size_t VGA_HEIGHT = 25;

size_t terminal_row;
size_t terminal_column;
uint8_t terminal_color;
uint16_t* terminal_buffer;


void terminal_initialize(void)
{
  terminal_row = 0;
  terminal_column = 0;
  terminal_color = 0x07; /* light gray text on black background */
  terminal_buffer = (uint16_t*) 0xB8000;
  /* clear VGA buffer, setting every element to space */
  for (size_t y = 0; y < VGA_HEIGHT; y++) {
    for (size_t x = 0; x < VGA_WIDTH; x++) {
      const size_t index = y * VGA_WIDTH + x;
      terminal_buffer[index] = ((uint16_t) ' ') | ((uint16_t) terminal_color << 8);
    }
  }
}

void print_section_info(uint32_t initial_sp)
{
  puts("sym\tstart\t\tend\n");
  puts(".text\t");
  puthex((uint32_t)__text_start);
  putchar('\t');
  puthex((uint32_t)__text_end);
  putchar('\n');
  puts(".data\t");
  puthex((uint32_t)__data_start);
  putchar('\t');
  puthex((uint32_t)__data_end);
  putchar('\n');
  puts(".rodata\t");
  puthex((uint32_t)__rodata_start);
  putchar('\t');
  puthex((uint32_t)__rodata_end);
  putchar('\n');
  puts(".bss\t");
  puthex((uint32_t)__bss_start);
  putchar('\t');
  puthex((uint32_t)__bss_end);
  putchar('\n');
  puts("initial_sp\t");
  puthex(initial_sp);
  putchar('\n');
}

extern void init_gdt(uint32_t initial_sp);
extern void init_idt();
extern void set_kernel_stack(uint32_t stack);
extern void multiboot_memmap_read();
extern void init_multiboot2(MultibootInfo *mbinfo);
extern uint32_t multiboot_frame_alloc();

void another_ring3()
{
  //puts("aasdfsafdsadf!\n");
  asm volatile("int $0x3");
  while(1);
}

void ring3_app()
{
  //puts("ring 3\n");
  int n = syscall(0, 1, 2, 3);
  
  //puthex(n);
  while(1);
}

void pit_init()
{
  outb(0x43, 0x36); // PIT_CTL -> TIMER0 | WRITE | SQUARE_WAVE
  uint16_t f = 1193182 / 100; // 100 ticks per second
  outb(0x40, f & 0xFF); // TIMER0_CTL -> lower freq
  outb(0x40, (f >> 8) & 0xFF); // TIMER0_CTL -> upper freq
}


void kernel_main(uint32_t multiboot_magic, void *mbhead, uint32_t initial_sp)
{
  if (multiboot_magic != 0x2BADB002)
    kernel_fault("incorrect multiboot magic number");
  
  terminal_initialize();
  
  SectionData sd = {
    .text_start = (uint32_t)__text_start,
    .text_end = (uint32_t)__text_end,
    .data_start = (uint32_t)__data_start,
    .data_end = (uint32_t)__data_end,
    .rodata_start = (uint32_t)__rodata_start,
    .rodata_end = (uint32_t)__rodata_end,
    .bss_start = (uint32_t)__bss_start,
    .bss_end = (uint32_t)__bss_end,
  };
  sections_init(&sd);
  
  init_multiboot(mbhead);
  
  
  outb(0x64, 0xAD);
  outb(0x64, 0xA7);
  kbd_init();
  
  init_idt();
  init_gdt(initial_sp);
  
  scheduler_init();
  
  pic_remap(0x20, 0x28);
  pit_init();
  
  multiboot_memmap_read();
  print_section_info(initial_sp);
  
  // init process has high priority until it is finished
  // doing init things, so set its priority to 1. TODO:
  // add a special, super-secret system call that the
  // init process will use to change its priority to
  // something super low. When its prio gets set super
  // low, it becomes the idle process.
  
  int pid = sched_new_kernel_process(1, 0, ring3_app);
  Process *proc = sched_get_process(pid);
  
  int pid2 = sched_new_kernel_process(2, 0, another_ring3);
  Process *p2 = sched_get_process(pid2);
  readyqueue_add(p2);
  
  outb(PIC1_DATA, 0xFC);
  asm volatile("sti");
  sched_start(proc);
}
