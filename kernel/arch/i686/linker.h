#ifndef __KERNEL_LINKER_H__
#define __KERNEL_LINKER_H__

#include <stddef.h>
#include <stdint.h>

/* variables corresponding to values in the linker script */
extern uint32_t __text_start[];
extern uint32_t __user_start[];
extern uint32_t __rodata_start[];
extern uint32_t __data_start[];
extern uint32_t __bss_start[];
extern uint32_t __text_end[];
extern uint32_t __user_end[];
extern uint32_t __rodata_end[];
extern uint32_t __data_end[];
extern uint32_t __bss_end[];


#endif
