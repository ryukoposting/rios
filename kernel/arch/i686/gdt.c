#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <page.h>
#include <context.h>
#include <arch/i686/arch_context.h>

#define GDT_LENGTH 6

#define GDT_TYPE_RING0_CODE 0x9A
#define GDT_TYPE_RING0_DATA 0x92
#define GDT_TYPE_RING3_CODE 0xFA
#define GDT_TYPE_RING3_DATA 0xF2
#define GDT_FLAG_4K_LIM 0x8
#define GDT_FLAG_32BIT 0x4

extern void tss_flush();

typedef union Gdt {
  struct {
    uint32_t loword;
    uint32_t hiword;
  };
  struct {
    uint16_t lim_0_15;
    uint16_t base_0_15;
    uint8_t base_16_23;
    uint8_t type;
    uint8_t lim_16_19 : 4;
    uint8_t flags : 4;
    uint8_t base_24_31;
  };
} __attribute__((packed)) Gdt;

typedef struct GdtPtr {
  uint16_t limit;
  uint32_t address;
} __attribute__((packed)) GdtPtr;


static Gdt gdt[GDT_LENGTH] = {};
GdtPtr gdtptr;

static Tss *tss;

static int gdt_allocn;

static inline void set_base(Gdt *g, uint32_t b)
{
  g->base_0_15 = b & 0xFFFF;
  g->base_16_23 = (b >> 16) & 0xFF;
  g->base_24_31 = (b >> 24) & 0xFF;
}

static inline void set_limit(Gdt *g, uint32_t l)
{
  g->lim_0_15 = l & 0xFFFF;
  g->lim_16_19 = (l >> 16) & 0xF;
}

static void flush_gdt()
{
  gdtptr.address = (uint32_t)gdt;
  gdtptr.limit = (GDT_LENGTH * 8) - 1;
  asm volatile (
    "lgdt %0\n\t": : "m" (gdtptr): "memory"
  );
  asm volatile (
    "mov $0x10, %%ax\n\t"
    "mov %%ax, %%ds\n\t"
    "mov %%ax, %%es\n\t"
    "mov %%ax, %%fs\n\t"
    "mov %%ax, %%gs\n\t"
    "mov %%ax, %%ss\n\t"
    ::: "ax"
  );
}

static void set_tss(int32_t num, uint16_t ss0, uint16_t esp0)
{
  uint32_t base = (uint32_t)tss;
  uint32_t limit = base + sizeof(tss);
  
  set_base(&gdt[num], base);
  set_limit(&gdt[num], limit);
  gdt[num].type = 0xE9;
  gdt[num].flags = 0;
  
  memset(tss, 0, sizeof(tss));
  tss->ss0 = ss0;
  tss->esp0 = esp0;
  // specifies GDT 1 (which is at offset 8) as the entry point to kernel
  // the 0x3 is the requested privelege level
  tss->cs = 0x08 | 0x03;
  // specifies GDT 2 (which is at offset 16) as the entry point to the
  // kernel. 0x3 is requested privelege level
  tss->ss = tss->ds = tss->es = tss->fs = tss->gs = 0x10 | 0x3;
}

void set_kernel_stack(uint32_t stack)
{
  tss->esp0 = stack;
}


void init_gdt(uint32_t initial_sp)
{
  tss = alloc_page(1);
  
  //redirection_tss = &tss[1];
  
  gdt[0].loword = 0;
  gdt[0].hiword = 0;
  
  set_base(&gdt[1], 0x00000000);
  set_limit(&gdt[1], 0xFFFFF);
  gdt[1].type = GDT_TYPE_RING0_CODE;
  gdt[1].flags = GDT_FLAG_4K_LIM | GDT_FLAG_32BIT;
  
  set_base(&gdt[2], 0x00000000);
  set_limit(&gdt[2], 0xFFFFF);
  gdt[2].type = GDT_TYPE_RING0_DATA;
  gdt[2].flags = GDT_FLAG_4K_LIM | GDT_FLAG_32BIT;
  
  set_base(&gdt[3], 0x00000000);
  set_limit(&gdt[3], 0xFFFFF);
  gdt[3].type = GDT_TYPE_RING3_CODE;
  gdt[3].flags = GDT_FLAG_4K_LIM | GDT_FLAG_32BIT;
  
  set_base(&gdt[4], 0x00000000);
  set_limit(&gdt[4], 0xFFFFF);
  gdt[4].type = GDT_TYPE_RING3_DATA;
  gdt[4].flags = GDT_FLAG_4K_LIM | GDT_FLAG_32BIT;
  
  set_tss(5, 0x10, initial_sp);
  
  //redirection_selector = 6;
  //set_base(&gdt[6], (uint32_t)redirection_tss);
  //set_limit(&gdt[6], 0xFFFF);
  
  gdt_allocn = 6;
  
  flush_gdt();
  tss_flush();
  //asm("int $0x3");
}


