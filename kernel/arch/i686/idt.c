#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <vmem.h>
#include <keyboard.h>
#include <scheduler.h>
#include <context.h>
#include <readyqueue.h>
#include <arch/i686/arch_context.h>
#include "interrupts.h"
#include "pic.h"

#define IDT_LENGTH 256

/* a normal interrupt that drops into ring 0 */
#define IDT_TYPEATTR_RING0_32BIT_INTERRUPT 0x8E
/* user-level interrupt, untested so far */
#define IDT_TYPEATTR_RING3_32BIT_TRAP 0xEF

typedef union IdtEntry {
  struct {
    uint32_t loword;
    uint32_t hiword;
  };
  struct {
    uint16_t offset_0_15;
    uint16_t selector;
    uint8_t zero;
    uint8_t type_attr;
    uint16_t offset_16_31;
  };
} __attribute__((packed)) IdtEntry;

typedef struct IdtPtr {
  uint16_t limit;
  uint32_t address;
} __attribute__((packed)) IdtPtr;


static IdtEntry idt[IDT_LENGTH] = {};
static IdtPtr idtptr;


// assumes selector 8, aka GDT entry 1 (kernel code segment)
static inline void set_idt_entry(int index, void (*handle)(), uint8_t typeattr)
{
  idt[index].offset_0_15 = ((uint32_t) handle) & 0xFFFF;
  idt[index].offset_16_31 = (((uint32_t) handle) >> 16) & 0xFFFF;
  idt[index].selector = 0x8;
  idt[index].zero = 0;
  idt[index].type_attr = typeattr;
}


static void flush_idt()
{
  idtptr.address = (uint32_t)idt;
  idtptr.limit = (IDT_LENGTH * 8) - 1;
  asm volatile (
    "lidt %0\n\t": : "m" (idtptr)
  );
}

/* every exception with a KFE is either a real, kernel-level failure,
   or I just haven't implemented a proper exception handler yet. */
KERNEL_FAULT_EXCEPTION(1, "Debug exception")
KERNEL_FAULT_EXCEPTION(2, "NMI")
KERNEL_FAULT_EXCEPTION(3, "Breakpoint exception")
KERNEL_FAULT_EXCEPTION(4, "Overflow exception")
KERNEL_FAULT_EXCEPTION(5, "Bounds Check exception")
KERNEL_FAULT_EXCEPTION(6, "Invalid Opcode")
KERNEL_FAULT_EXCEPTION(7, "Device Not Available")
KERNEL_FAULT_EXCEPTION(8, "Double Fault exception")
KERNEL_FAULT_EXCEPTION(9, "Coprocessor Segment Overrun exception")
KERNEL_FAULT_EXCEPTION(10, "Invalid TSS exception")
KERNEL_FAULT_EXCEPTION(11, "Segment Not Present exception")
KERNEL_FAULT_EXCEPTION(12, "Stack exception")
KERNEL_FAULT_EXCEPTION(15, "Unknown exception")
KERNEL_FAULT_EXCEPTION(16, "Floating-Point Coprocessor exception")

EXCEPTION_ENTRY(0)
void exception_0_handler(CoreDump cd)
{
  puts("divide by zero error!");
  while(1);
}

EXCEPTION_ENTRY(13)
void exception_13_handler(CoreDump cd)
{
  puts("general protection fault");
  while(1);
}

EXCEPTION_ENTRY(14)
void exception_14_handler(CoreDump cd)
{
  /* cd.exception_code has important flags:
    bit 0: 0->fault caused by non-present page
           1->fault caused by page protection violation
    bit 1: 0->caused by read access
           1->caused by write access
    bit 2: 0->didn't occur in ring 3
           1->occurred in ring 3
    bit 3: only means something if we're using big pages and/or PAE
    bit 4: 1->caused by an instruction fetch (probably not important?) */
  uint32_t fault_addr;      // the address that someone tried to access
  uint32_t fault_directory; // their page directory
  asm(
    "mov %%cr2, %%eax\n\t"
    "mov %%eax, %0\n\t"
    "mov %%cr3, %%eax\n\t"
    "mov %%eax, %1\n\t"
    : "=m"(fault_addr), "=m"(fault_directory)
    :: "%eax"
  );
  puts("\npage fault:\n  fault_addr=\t");
  puthex(fault_addr);
  puts("\n  fault_directory=\t");
  puthex(fault_directory);
  puts("\n  exception_code=\t");
  puthex(cd.exception_code);
  putchar('\n');
  while(1);
}

extern void another_ring3();

static int k = 0;
EXCEPTION_ENTRY(0x80)
void exception_0x80_handler(CoreDump cd)
{
  /* funct = eax, arg1 = edx, arg2 = ecx, arg3 = ebx */
  puts("SYSCALL ");
//  if (cd.eax == 0 && k == 0) {
//    int pid = sched_new_kernel_process(2, 0, another_ring3);
//    Process *proc = sched_get_process(pid);
//    readyqueue_add(proc);
//    k = 1;
//  }
  putnum(cd.eax);
  putchar(' ');
  putnum(cd.edx);
  cd.eax = 0xBEEEEEEF;
}

EXCEPTION_ENTRY(0x21)
void exception_0x21_handler(CoreDump cd)
{
  static uint8_t kbuf[8] = {};
  static int len = 0;
  
  pic_sendeoi(1);
  
  uint8_t b = inb(0x60);
  kbuf[len++] = b;
  
  int r;
  KeyData kdata;
  
  switch (kbd_continuation(kbuf, len)) {
  case 0:
    if (!kbd_handle(&kdata, kbuf, len)) {
      if (kdata.press && (kdata.printable || (kdata.asciirepr == 0x08)))
        putchar(kdata.asciirepr);
    } else {
      puts("invalid keycode\n");
    }
    len = 0;
    break;
  case 1:
    break;
  default:
    kernel_fault("keyboard exception handler");
  }
}

CONTEXT_SWITCH_ENTRY(0x20)
Process *exception_0x20_handler(CoreDump cd)
{
  pic_sendeoi(0);
  
  Process *current = sched_get_current_process();
  Process *switchto = 0;
  
  if (current) {
    //puts("ctsw\n");
    sched_tick();
    readyqueue_add(current);
    Process *n = readyqueue_pop();
    if (current != n) {
      switchto = n;
      // need to perform a context switch, save tss
      // thanks serenity
      current->context->tss.gs = cd.gs;
      current->context->tss.fs = cd.fs;
      current->context->tss.es = cd.es;
      current->context->tss.ds = cd.ds;
      current->context->tss.edi = cd.edi;
      current->context->tss.esi = cd.esi;
      current->context->tss.ebp = cd.ebp;
      current->context->tss.ebx = cd.ebx;
      current->context->tss.edx = cd.edx;
      current->context->tss.ecx = cd.ecx;
      current->context->tss.eax = cd.eax;
      current->context->tss.eip = cd.eip;
      current->context->tss.cs = cd.cs;
      current->context->tss.eflags = cd.eflags;

      // Compute process stack pointer.
      // Add 16 for CS, EIP, EFLAGS, exception code (interrupt mechanic)
      current->context->tss.esp = cd.esp + 16;
      current->context->tss.ss = cd.ss;
      
      sched_set_current_process(switchto);
    }
  }
  // the actual switchoff needs to happen either here or in the ASM
  return switchto;
}

void init_idt()
{
  set_idt_entry(0, exception_0_entry, IDT_TYPEATTR_RING0_32BIT_INTERRUPT);
  set_idt_entry(1, kernel_fault_1, IDT_TYPEATTR_RING0_32BIT_INTERRUPT);
  set_idt_entry(2, kernel_fault_2, IDT_TYPEATTR_RING0_32BIT_INTERRUPT);
  set_idt_entry(3, kernel_fault_3, IDT_TYPEATTR_RING0_32BIT_INTERRUPT);
  set_idt_entry(4, kernel_fault_4, IDT_TYPEATTR_RING0_32BIT_INTERRUPT);
  set_idt_entry(5, kernel_fault_5, IDT_TYPEATTR_RING0_32BIT_INTERRUPT);
  set_idt_entry(6, kernel_fault_6, IDT_TYPEATTR_RING0_32BIT_INTERRUPT);
  set_idt_entry(7, kernel_fault_7, IDT_TYPEATTR_RING0_32BIT_INTERRUPT);
  set_idt_entry(8, kernel_fault_8, IDT_TYPEATTR_RING0_32BIT_INTERRUPT);
  set_idt_entry(9, kernel_fault_9, IDT_TYPEATTR_RING0_32BIT_INTERRUPT);
  set_idt_entry(10, kernel_fault_10, IDT_TYPEATTR_RING0_32BIT_INTERRUPT);
  set_idt_entry(11, kernel_fault_11, IDT_TYPEATTR_RING0_32BIT_INTERRUPT);
  set_idt_entry(12, kernel_fault_12, IDT_TYPEATTR_RING0_32BIT_INTERRUPT);
  set_idt_entry(13, exception_13_entry, IDT_TYPEATTR_RING0_32BIT_INTERRUPT);
  set_idt_entry(14, exception_14_entry, IDT_TYPEATTR_RING0_32BIT_INTERRUPT);
  set_idt_entry(15, kernel_fault_15, IDT_TYPEATTR_RING0_32BIT_INTERRUPT);
  set_idt_entry(16, kernel_fault_16, IDT_TYPEATTR_RING0_32BIT_INTERRUPT);
  set_idt_entry(0x20, exception_0x20_entry, IDT_TYPEATTR_RING0_32BIT_INTERRUPT);
  set_idt_entry(0x21, exception_0x21_entry, IDT_TYPEATTR_RING0_32BIT_INTERRUPT);
  set_idt_entry(0x80, exception_0x80_entry, IDT_TYPEATTR_RING3_32BIT_TRAP);
  flush_idt();
}
