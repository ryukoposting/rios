#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include "pic.h"

void pic_sendeoi(uint8_t irq)
{
  if (irq >= 8)
    outb(PIC2_COMMAND, PIC_EOI);
  outb(PIC1_COMMAND, PIC_EOI);
}

void pic_remap(int offset1, int offset2)
{
  outb(PIC1_COMMAND, ICW1_INIT | ICW1_ICW4); // starts the initialization sequence (in cascade mode)
  //waitb();
  outb(PIC2_COMMAND, ICW1_INIT | ICW1_ICW4);
  //waitb();
  outb(PIC1_DATA, offset1); // ICW2: Master PIC vector offset
  //waitb();
  outb(PIC2_DATA, offset2); // ICW2: Slave PIC vector offset
  //waitb();
  outb(PIC1_DATA, 4); // ICW3: tell Master PIC that there is a slave PIC at IRQ2 (0000 0100)
  //waitb();
  outb(PIC2_DATA, 2); // ICW3: tell Slave PIC its cascade identity (0000 0010)
  //waitb();
 
  outb(PIC1_DATA, ICW4_8086);
  //waitb();
  outb(PIC2_DATA, ICW4_8086);
  //waitb();
  
  // enable PIT, keyboard, and PIC2 IRQs. all others disabled.
  outb(PIC1_DATA, 0xFF);
  outb(PIC2_DATA, 0xFF);
}

