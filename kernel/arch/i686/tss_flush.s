.section .text
.globl tss_flush
tss_flush:
  mov $0x2B, %ax
  ltr %ax
  ret


.globl load_context
load_context:
  mov 4(%esp), %eax
  mov 8(%esp), %ebx
  mov 12(%esp), %ecx
repeat:
  jmp repeat
