#include <stdio.h>
#include "termio.h"

int puts(const char *s)
{
  for (size_t i = 0; s[i]; ++i)
    putchar(s[i]);
  return 0;
}
