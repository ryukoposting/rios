#include <stdlib.h>
#include <stdint.h>

unsigned int syscall(int function, int arg1, int arg2, int arg3)
{
  unsigned int result;
  asm volatile("int $0x80"
    : "=a"(result)
    : "a"(function), 
      "d"((uint32_t)arg1),
      "c"((uint32_t)arg2),
      "b"((uint32_t)arg3)
    : "memory");
  return result;
}
