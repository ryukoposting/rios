#include <kernel.h>
#include <stdio.h>
#include "termio.h"

void kernel_fault(char const *msg)
{
  terminal_row = 0;
  terminal_column = 0;
  terminal_setcolor(vga_entry_color(VGA_COLOR_WHITE, VGA_COLOR_RED));
  puts("KERNEL FAULT\n");
  puts(msg);
  asm volatile("cli; hlt");
  while(1);
}

void kernel_fault_errcode(char const *msg, unsigned int errcode)
{
  terminal_row = 0;
  terminal_column = 0;
  terminal_setcolor(vga_entry_color(VGA_COLOR_WHITE, VGA_COLOR_RED));
  puts("KERNEL FAULT\n");
  puts(msg);
  putchar('\n');
  puthex(errcode);
  asm volatile("cli; hlt");
  while(1);
}
