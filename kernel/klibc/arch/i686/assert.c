#include <assert.h>
#include <stdio.h>
#include <kernel.h>
#include "termio.h"

#if !defined(NDEBUG)
void __rios_assert(int expression, char *expr)
{
  if (!expression) {
    kernel_fault(expr);
  }
}
#endif
