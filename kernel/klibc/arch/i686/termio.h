#ifndef __TERMIO_H__
#define __TERMIO_H__

#include <stdint.h>
#include <string.h>

typedef enum VgaColor {
  VGA_COLOR_BLACK = 0,
  VGA_COLOR_BLUE = 1,
  VGA_COLOR_GREEN = 2,
  VGA_COLOR_CYAN = 3,
  VGA_COLOR_RED = 4,
  VGA_COLOR_MAGENTA = 5,
  VGA_COLOR_BROWN = 6,
  VGA_COLOR_LIGHT_GREY = 7,
  VGA_COLOR_DARK_GREY = 8,
  VGA_COLOR_LIGHT_BLUE = 9,
  VGA_COLOR_LIGHT_GREEN = 10,
  VGA_COLOR_LIGHT_CYAN = 11,
  VGA_COLOR_LIGHT_RED = 12,
  VGA_COLOR_LIGHT_MAGENTA = 13,
  VGA_COLOR_LIGHT_BROWN = 14,
  VGA_COLOR_WHITE = 15
} VgaColor;

extern const size_t VGA_WIDTH;
extern const size_t VGA_HEIGHT;
extern size_t terminal_row;
extern size_t terminal_column;
extern uint8_t terminal_color;
extern uint16_t* terminal_buffer;

static inline uint8_t vga_entry_color(uint8_t fg, uint8_t bg) 
{
  return fg | bg << 4;
}

static inline uint16_t vga_entry(unsigned char uc, uint8_t color) 
{
  return (uint16_t) uc | (uint16_t) color << 8;
}

static inline void terminal_setcolor(uint8_t color) 
{
  terminal_color = color;
}

static inline void terminal_putentryat(char c, uint8_t color, size_t x, size_t y) 
{
  const size_t index = y * VGA_WIDTH + x;
  terminal_buffer[index] = vga_entry(c, color);
}

static inline uint16_t terminal_get(size_t x, size_t y)
{
  return terminal_buffer[y * VGA_WIDTH + x] & 0xFF;
}

static inline void terminal_shiftup()
{
  size_t i;
  for (i = 0; i < VGA_WIDTH * (VGA_HEIGHT - 1); i += 1)
    terminal_buffer[i] = terminal_buffer[i + VGA_WIDTH];
  for (; i < VGA_WIDTH * VGA_HEIGHT; i += 1)
    terminal_buffer[i] = vga_entry(' ', terminal_color);
}

#endif
