#include <stdio.h>
#include "termio.h"

int putchar(int c) 
{
  int i;
  switch (c) {
  case '\r':
    terminal_column = 0;
    break;
  
  case '\n':
    terminal_column = 0;
    if (++terminal_row >= VGA_HEIGHT) {
      terminal_shiftup();
      terminal_row = VGA_HEIGHT - 1;
    }
    break;
  
  case '\t':
    terminal_column = (terminal_column & ~(0x7)) + 8;
    if (terminal_column >= VGA_WIDTH) {
      terminal_column = terminal_column % VGA_WIDTH;
      if (++terminal_row >= VGA_HEIGHT) {
        terminal_shiftup();
        terminal_row = VGA_HEIGHT - 1;
      }
    }
    break;
  
  case 0x08:
    if (terminal_column > 0) {
      terminal_column -= 1;
    } else if (terminal_row > 0) {
      terminal_row -= 1;
      terminal_column = VGA_WIDTH - 1;
      while ((terminal_get(terminal_column, terminal_row) == ' ') &&
          ((terminal_column != 0) || (terminal_row != 0))) {
        if (terminal_column > 0) {
          terminal_column -= 1;
        } else if (terminal_row > 0) {
          terminal_row -= 1;
          terminal_column = VGA_WIDTH - 1;
        }
      }
    }
    
    terminal_putentryat(' ', terminal_color, terminal_column, terminal_row);
    break;
    
  default:
    terminal_putentryat((unsigned char)c, terminal_color, terminal_column, terminal_row);
    if (++terminal_column >= VGA_WIDTH) {
      terminal_column = 0;
      if (++terminal_row >= VGA_HEIGHT) {
        terminal_shiftup();
        terminal_row = VGA_HEIGHT - 1;
      }
    }
    break;
  }
  
  return c;
}
