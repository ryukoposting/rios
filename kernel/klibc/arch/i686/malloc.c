#include <stdlib.h>
#include <stddef.h>
#include <assert.h>
#include <page.h>
//#define MALLOC_DEBUG
#ifdef MALLOC_DEBUG
#include <stdio.h>
#endif

/* malloc and free are based heavily on the K&R implementation,
 but malloc includes extra code for finding a perfect fit, 
 a buddy fit, and a quasi-buddy fit. A quasi-buddy fit is within
 a few bytes of being a true buddy fit. */

typedef union Header {
  struct {
    union Header *ptr;
    size_t size;
  } s;
  long int _align;
} __attribute__((aligned(4))) Header;

static Header *freep = NULL;

void *malloc(size_t nbytes)
{
#ifdef MALLOC_DEBUG
  puts("malloc: request to allocate 0x");
  puthex(nbytes);
  puts(" bytes\n");
#endif
  Header *p, *prevp;
  size_t nunits;
  void *cp;

  nunits = (nbytes + sizeof(Header) - 1) / sizeof(Header) + 1;

  if ((prevp = freep) == NULL) {
    freep = prevp = (Header *)alloc_page(1);
    freep->s.size = 4096 / sizeof(Header);
    freep->s.ptr = freep;
  }

  Header *non_perfect_p = NULL;
  Header *non_perfect_prevp = NULL;

  char non_perfect_found = 0;
  char buddy_found = 0;
  char bud4_found = 0;

  for (p = prevp->s.ptr; ; prevp = p, p = p->s.ptr) {
    if (p->s.size == nunits) {
      /* perfect fit */
#ifdef MALLOC_DEBUG
        puts("malloc: found perfect fit at base addr ");
        puthex(p);
        putchar('\n');
#endif
      prevp->s.ptr = p->s.ptr;
      freep = prevp;
      return (void *)(p + 1);
    } else if (
      (p->s.size > nunits) && 
      ((p->s.size & (~0b1)) == ((nunits << 1) & (~0b1)))) {
      /* Found a perfect buddy fit, unless we find a true perfect fit elsewhere,
       this is probably our best option. */
#ifdef MALLOC_DEBUG
        puts("malloc: found a buddy fit at base addr ");
        puthex(p);
        putchar('\n');
#endif
      non_perfect_p = p;
      non_perfect_prevp = prevp;
      non_perfect_found = 1;
      buddy_found = 1;
    } else if (
      (p->s.size > nunits) && 
      ((p->s.size & (~0b11)) == ((nunits << 1) & (~0b11))) && 
      (buddy_found == 0)) {
      /* This is pretty close to being a proper buddy fit */
#ifdef MALLOC_DEBUG
        puts("malloc: found a near-buddy fit at base addr ");
        puthex(p);
        putchar('\n');
#endif
      non_perfect_p = p;
      non_perfect_prevp = prevp;
      non_perfect_found = 1;
      bud4_found = 1;
    } else if (
      (p->s.size > nunits) &&
      (non_perfect_found == 1) &&
      (buddy_found == 0) &&
      (bud4_found == 0)) {
      /* found another fit that isn't a buddy or near-buddy fit... is it a closer
       fit than the last one we found? */
#ifdef MALLOC_DEBUG
        puts("malloc: found a fit at base addr ");
        puthex(p);
        putchar('\n');
#endif
      assert(non_perfect_p != NULL); // illegal state in closestfit in malloc
      if (p->s.size < non_perfect_p->s.size) {
        non_perfect_p = p;
        non_perfect_prevp = prevp;
        non_perfect_found = 1;
      }
    } else if (
      (p->s.size > nunits) &&
      (non_perfect_found == 0) &&
      (buddy_found == 0) &&
      (bud4_found == 0)) {
      /* keep track of the first usable space, in case
        it's the only one that's suitable */
      non_perfect_p = p;
      non_perfect_prevp = prevp;
      non_perfect_found = 1;
    }

    if (p == freep) {
      // made it to the end of the open slots, did we find one that we
      // considered imperfect-but-usable?
      if (non_perfect_found == 1) {
        // sub-optimal spot found, just use it
//        p = non_perfect_p + nunits;
//        p->s.ptr = non_perfect_p->s.ptr;
//        p->s.size = non_perfect_p->s.size - nunits;
//        non_perfect_prevp->s.ptr = p;
//        non_perfect_p->s.size = nunits;
        p = non_perfect_p + nunits;
        non_perfect_prevp->s.ptr = p;
        p->s.ptr = non_perfect_p->s.ptr;
        p->s.size = non_perfect_p->s.size - nunits;
        non_perfect_p->s.size = nunits;
        
        freep = p;
        return (void *)(non_perfect_p + 1);
      } else {
        // no suitable spot found, try to get more space
        size_t npages = 1 + ((nunits * sizeof(Header)) / 4096);
        cp = (Header *)alloc_page(npages);
        if (cp == 0) {
          return NULL;
        } else {
          p = (Header *) cp;
          p->s.size = (npages * 4096) / sizeof(Header);
          free((void *) (p + 1));
          p = freep;
        }
      }
    }
  }
  assert(0);
}


void free(void *ap)
{
  Header *bp, *p;
  bp = (Header *) ap - 1;
#ifdef MALLOC_DEBUG
  puts("free: freeing base addr ");
  puthex((int)bp);
  putchar('\n'); 
#endif

  // stop once p < bp and bp < p->next
  for (p = freep; ; p = p->s.ptr) {
    if ((p <= bp) && (p->s.ptr >= bp))
      break;
    if ((p >= p->s.ptr) && ((p < bp) || (p->s.ptr > bp)))
      break;
  }

  // combine p->s.ptr with bp if they are directly adjacent
  if (bp + bp->s.size == p->s.ptr) {
    bp->s.size += p->s.ptr->s.size;
    bp->s.ptr = p->s.ptr->s.ptr;
  } else {
    bp->s.ptr = p->s.ptr;
  }
  
  // combine p with bp if they are directly adjacent
  if (p + p->s.size == bp) {
    p->s.size += bp->s.size;
    p->s.ptr = bp->s.ptr;
  } else {
    p->s.ptr = bp;
  }
  
  freep = p;
}

void print_freep()
{
  // freep->s.ptr is where malloc starts searching
  Header *p = freep->s.ptr;
  do {
    puthex(p);
    putchar(' ');
    puthex(p->s.size * sizeof(Header));
    putchar('\n');
    p = p->s.ptr;
  } while (p != freep->s.ptr);
}
