#ifndef __SECTIONS_H__
#define __SECTIONS_H__

#include <stddef.h>
#include <stdint.h>

typedef struct SectionData SectionData;

struct SectionData {
  size_t text_start;
  size_t text_end;
  size_t data_start;
  size_t data_end;
  size_t rodata_start;
  size_t rodata_end;
  size_t bss_start;
  size_t bss_end;
};

void sections_init(SectionData *sd);

SectionData *sections_get_sectiondata();

int sections_region_reserved(uint64_t addr, uint64_t len);

#endif
