#include <kernel.h>
#include <stdio.h>
#include <assert.h>
#include <sections.h>
#include "multiboot.h"

MultibootInfo *multiboot_info;
static uint32_t next_free_frame;
static uint32_t reserved_start;
static uint32_t reserved_end;

static uint64_t next_addr;
static uint64_t end_entry;
static MultibootMmapEntry *cur_entry;


void init_multiboot(MultibootInfo *mbinfo)
{
  multiboot_info = mbinfo;
  if (!(multiboot_info->flags & (1 << 6)))
    kernel_fault("multiboot did not provide a memory map");
  
  uint32_t *maddr = (uint32_t *)multiboot_info->mmap_addr;
  uint32_t *mmap_end = (uint32_t*) ((uint32_t)maddr + multiboot_info->mmap_length);
  MultibootMmapEntry *entry;
  uint64_t l;
  for (entry; maddr < mmap_end;
      maddr = (uint32_t*) ((uint32_t)maddr + entry->size + sizeof(int*))) {
    entry = (MultibootMmapEntry*)maddr;
    if (entry->type == MMAP_ENTRYTYPE_AVAILABLE) {
      next_addr = entry->addr_hi;
      next_addr <<= 32;
      next_addr |= entry->addr_low;
      l = entry->len_hi;
      l <<= 32;
      l |= entry->len_low;
      if (!sections_region_reserved(next_addr, l)) {
        cur_entry = entry;
        end_entry = next_addr + l;
        break;
      }
    }
  }
  if (next_addr == 0)
    next_addr += 4096;
}


void multiboot_memmap_read()
{
  uint32_t *maddr = (uint32_t *)multiboot_info->mmap_addr;
  uint32_t *mmap_end = (uint32_t*) ((uint32_t)maddr + multiboot_info->mmap_length);
  MultibootMmapEntry *entry;
  for (entry; maddr < mmap_end;
      maddr = (uint32_t*) ((uint32_t)maddr + entry->size + sizeof(int*))) {
    entry = (MultibootMmapEntry*)maddr;
    //if (entry->size) {
      puthex(entry->size);
      putchar(' ');
      puthex(entry->addr_hi);
      puthex(entry->addr_low);
      putchar(' ');
      puthex(entry->len_hi);
      puthex(entry->len_low);
      putchar(' ');
      puthex(entry->type);
      putchar('\n');
    //}
  }
}


// TODO: add in addr_hi and len_hi to support full-width addresses
uint32_t multiboot_frame_allocate()
{
  static int donezo = 0;
  if (donezo)
    return 0;
  
  while (sections_region_reserved(next_addr, 4096) || (next_addr + 4096 > end_entry)) {
    next_addr += 4096;
    
    if (next_addr > end_entry) {
      uint32_t *maddr = (uint32_t *)multiboot_info->mmap_addr;
      uint32_t *mmap_end = (uint32_t*) ((uint32_t)maddr + multiboot_info->mmap_length);
      MultibootMmapEntry *next_entry = cur_entry;
      uint32_t n = (uint32_t)cur_entry;
      do {
        puts("frame_alloc: reading next open frame\n");
        n += next_entry->size + sizeof(int*);
        next_entry = (MultibootMmapEntry*)n;
      } while ((next_entry < mmap_end) && (next_entry->type != MMAP_ENTRYTYPE_AVAILABLE));
      
      if (next_entry >= mmap_end) {
        donezo = 1;
        return 0;
      }
      
      cur_entry = next_entry;
      end_entry = cur_entry->addr_low + cur_entry->len_low;
      next_addr = cur_entry->addr_low;
    }
  }
  
  uint32_t result = next_addr;
  next_addr += 4096;
  return result;
}
