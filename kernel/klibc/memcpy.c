#include "string.h"
#include <stdint.h>

void *memcpy(void *dest, void const *src, size_t n)
{
  uint8_t *d = dest;
  uint8_t const *s = src;
  for (size_t i = 0; i < n; ++i)
    d[i] = s[i];
  return src;
}
