#ifndef __LIBC_ASSERT_H__
#define __LIBC_ASSERT_H__

#if defined(NDEBUG)
#define assert(ignore) ((void)0)
#define __rios_assert(ignore, iignore) ((void)0)
#else
#define assert(expr) __rios_assert(expr, "assertion error: " # expr)
void __rios_assert(int expression, char *expr);
#endif

#endif
