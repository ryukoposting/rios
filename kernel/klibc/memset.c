#include "stdlib.h"
#include "stddef.h"

void *memset(void *s, int c, size_t n)
{
  unsigned char *ss = s;
  for (int i = 0; i < n; ++i) {
    ss[i] = (unsigned char)c;
  }
  return s;
}
