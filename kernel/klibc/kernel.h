#ifndef __LIBC_KERNEL_H__
#define __LIBC_KERNEL_H__

void kernel_fault(char const *msg);
void kernel_fault_errcode(char const *msg, unsigned int errcode);

#endif
