#ifndef __LIBC_STDIO_H__
#define __LIBC_STDIO_H__
#include <stdint.h>
#include <stddef.h>

#ifdef __ARCH_i686
static inline void outb(uint16_t port, uint8_t val)
{
  asm volatile (
    "outb %0, %1"
    :
    : "a"(val), "Nd"(port));
}

static inline uint8_t inb(uint16_t port)
{
  uint8_t ret;
  asm volatile (
    "inb %1, %0"
    : "=a"(ret)
    : "Nd"(port)
  );
  return ret;
}

static inline void waitb()
{
  asm volatile (
    "outb %%al, $0x80"
    :
    : "a"(0)
  );
}
#endif

int putchar(int c);

int puts(const char *s);

/* prints a number in signed base 10 */
int putnum(int n);

/* prints a number in unsigned base 16 */
int puthex(int n);

#endif
