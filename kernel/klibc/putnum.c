#include <stdio.h>

// TODO: make a more optimal version of this
int putnum(int n)
{
  if (n < 0)
    putchar('-');
  
  char buf[16]; //TODO: make this adjust based on sizeof(int)
  int bn, i = 0;
  do {
    bn = n % 10;
    if (n > 0)
      buf[i] = '0' + bn;
    else
      buf[i] = '0' - bn;
    n /= 10;
    i += 1;
  } while (n != 0);

  while(i)
    putchar(buf[--i]);
  
  return 0;
}

int puthex(int n)
{
  int b = (sizeof(int) * 8) - 4;
  int o;
  char c;
  while (b >= 0) {
    o = (n >> b) & 0xF;
    c = '0' + o;
    if (o > 9)
      c += 7;
    putchar(c);
    b -= 4;
  }
  return 0;
}
