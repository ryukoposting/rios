#ifndef __KERNEL_MULTIBOOT_H__
#define __KERNEL_MULTIBOOT_H__

#include <stddef.h>
#include <stdint.h>

typedef struct MultibootHeader {
  uint32_t magic;
  uint32_t flags;
  /* magic + flags + checksum = 0 mod (2^32)*/
  uint32_t checksum;
  /* only valid if AOUT_KLUDGE flag is set */
  uint32_t header_addr;
  uint32_t load_addr;
  uint32_t load_end_addr;
  uint32_t bss_end_addr;
  uint32_t entry_addr;
  /* only valid if VIDEO_MODE flag is set */
  uint32_t mode_type;
  uint32_t width;
  uint32_t height;
  uint32_t depth;
} MultibootHeader;

typedef struct MultibootAoutSymTbl {
  uint32_t tabsize;
  uint32_t strsize;
  uint32_t addr;
  uint32_t reserved;
} MultibootAoutSymTbl;

typedef struct MultibootElfSecHeaderTbl {
  uint32_t num;
  uint32_t size;
  uint32_t addr;
  uint32_t shndx;
} MultibootElfSecHeaderTbl;

typedef struct MultibootInfo {
  uint32_t flags;
  /* bounds of available BIOS memory */
  uint32_t mem_lower;
  uint32_t mem_upper;
  /* "root" partition */
  uint32_t boot_device;
  /* kernel command line */
  uint32_t cmdline;
  /* boot-module list */
  uint32_t mods_count;
  uint32_t mods_addr;
  
  union {
    MultibootAoutSymTbl aout_sym;
    MultibootElfSecHeaderTbl elf_sec;
  };
  
  /* mmap buffer */
  uint32_t mmap_length;
  uint32_t mmap_addr;
  
  /* drive info buffer */
  uint32_t drives_length;
  uint32_t drives_addr;
  
  uint32_t romconf_tbl;
  
  uint32_t bootloader_name;
  
  uint32_t apm_tbl;
  
  /* video stuff */
  uint32_t vbe_control_info;
  uint32_t vbe_mode_info;
  uint16_t vbe_mode;
  uint16_t vbe_interface_seg;
  uint16_t vbe_interface_off;
  uint16_t vbe_interface_len;
  uint64_t framebuffer_addr;
  uint32_t framebuffer_pitch;
  uint32_t framebuffer_width;
  uint32_t framebuffer_height;
  uint8_t framebuffer_bpp;
  
  union {
    struct {
      uint32_t framebuffer_pallette_addr;
      uint16_t framebuffer_pallette_num_colors;
    };
    struct {
      uint8_t framebuffer_red_field_position;
      uint8_t framebuffer_red_mask_size;
      uint8_t framebuffer_green_field_position;
      uint8_t framebuffer_green_mask_size;
      uint8_t framebuffer_blue_field_position;
      uint8_t framebuffer_blue_mask_size;
    };
  };
} MultibootInfo;

typedef struct MultibootMmapEntry {
  uint32_t size;
  uint32_t addr_low;
  uint32_t addr_hi;
  uint32_t len_low;
  uint32_t len_hi;
  uint32_t type;
} __attribute__((packed)) MultibootMmapEntry;

typedef enum MultibootMmapEntryType {
  MMAP_ENTRYTYPE_AVAILABLE = 1,
  MMAP_ENTRYTYPE_RESERVED = 2,
  MMAP_ENTRYTYPE_ACPI_RECLAIMABLE = 3,
  MMAP_ENTRYTYPE_NVS = 4,
  MMAP_ENTRYTYPE_BADRAM = 5
} MultibootMmapEntryType;

extern MultibootInfo *multiboot_info;

void init_multiboot(MultibootInfo *mbinfo);

uint32_t multiboot_frame_allocate();


#endif
