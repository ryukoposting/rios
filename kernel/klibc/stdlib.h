#ifndef __LIBC_STDLIB_H__
#define __LIBC_STDLIB_H__

#include <stddef.h>

void *malloc(size_t size);

void free(void *ap);

unsigned int syscall(int function, int arg1, int arg2, int arg3);

#endif
