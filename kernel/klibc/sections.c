#include "sections.h"
#include <string.h>
#include <stdio.h>

static SectionData secdata;

size_t reserve_low;
size_t reserve_high;

void sections_init(SectionData *sd)
{
  memcpy(&secdata, sd, sizeof(secdata));
  
  reserve_low = secdata.text_start;
  reserve_high = secdata.text_end;
  
  uint32_t *arr = (uint32_t*)(&secdata);
  for (int i = 0; i < sizeof(secdata) / sizeof(size_t); ++i) {
    if (arr[i] < reserve_low)
      reserve_low = arr[i];
    else if (arr[i] > reserve_high)
      reserve_high = arr[i];
  }
  
  puts("reserve_low=");
  puthex(reserve_low);
  puts(" reserve_high=");
  puthex(reserve_high);
  putchar('\n');
}

SectionData *sections_get_sectiondata()
{
  return &secdata;
}

int sections_region_reserved(uint64_t addr, uint64_t len)
{  // think of this == as an XNOR
  return (addr > reserve_high) == ((addr + len - 1) < reserve_low);
}
