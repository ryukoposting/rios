#ifndef __ARCH_RQUEUE_H__
#define __ARCH_RQUEUE_H__

#include "../../scheduler.h"
#include "../../readyqueue.h"

void arch_proctable_init();

Process *arch_get_process(int pid);

/* get a proctbl entry that is currently unused. */
Process *arch_get_open_proc();

/* add a proctbl entry to the list of unused entries */
void arch_add_open_proc(Process *p);

int arch_get_pid(Process *proc);

#endif
