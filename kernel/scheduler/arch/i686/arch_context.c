#include <context.h>
#include <stdlib.h>
#include <stdio.h>

ContextData *context_new_contextdata()
{
  // TODO: make proper allocator for this
  // TODO: also make this actually do something
  ContextData *result = malloc(sizeof(ContextData));
  puts("context_new_contextdata: allocated ");
  putnum(sizeof(ContextData));
  puts(" bytes at address ");
  puthex(result);
  putchar('\n');
  result->tss.esp = 0;
  result->tss.ebp = 0;
  result->tss.eip = 0;
  return result;
}
