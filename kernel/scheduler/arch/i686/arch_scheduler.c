#include "arch_scheduler.h"
#include <stdio.h>

static Process *current_process;

extern void sched_begin_ring3(Process *proc);
extern void arch_proctable_init(); // arch_proctable.c

void arch_sched_init()
{
  current_process = NULL;
  arch_proctable_init();
}

Process *sched_get_current_process()
{
  return current_process;
}

void sched_set_current_process(Process *proc)
{
  current_process = proc;
}

void sched_start(Process *proc)
{
  current_process = proc;
  puts("starting...\n");
  sched_begin_ring3(proc);
}
