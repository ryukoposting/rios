.globl sched_begin_ring3
sched_begin_ring3:
  push %ebp
  mov %esp, %ebp
  
  mov 8(%esp), %eax   // eax = proc
  mov 16(%eax), %ebx
  mov 56(%ebx), %ecx  // ecx = ring3 esp
  mov 32(%ebx), %edx  // edx = ring3 eip
  mov 12(%eax), %ebx  // ebx = page directory
  
  mov %ebx, %cr3     // new page dir -> cr3
  mov %cr0, %eax     // enable paging
  or $0x80000000, %eax
  mov %eax, %cr0

  mov $0x23, %eax     // eax = ring3 data segment
  mov %eax, %ds
  mov %eax, %es
  mov %eax, %fs
  mov %eax, %gs
  
  push $0x23         // ring3 data segment
  push %ecx          // ring3 esp
  pushf
  push $0x1B         // ring3 code segment
  push %edx          // ring3 eip
  iret
