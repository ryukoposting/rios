#include "arch_scheduler.h"
#include <stdlib.h>
#include <assert.h>
#include <page.h>

/* proctbl is a page of 1024 pointers.
   each pointer is either null, or it goes to a page
   that contains 128 Process structs (these frames
   are referred to as "process frames").
   bits 7-16 describe the index within proctbl pointing to
     the correct process frame.
   bits 0-6 describe the index within that process frame
   
   this design allows for pids from 0 to 131071(0x1FFFF) */

// TODO: add static assertion to check that Process structure is 32 bytes

static Process **proctbl;
static Process *freeproc;


int arch_get_pid(Process *proc)
{
  Process *base = (int)proc & (~4095); // addr of page
  int upper = -1;
  for (int i = 0; i < 1024; ++i) {
    if (proctbl[i] == base) {
      upper = i;
      break;
    }
  }
  
  if (upper == -1)
    return -1;
  
  int lower = ((int)proc & 4095) >> 5;
  
  return lower | (upper << 5);
}

static void alloc_procframe(int i)
{
  assert(i >= 0);
  assert(i < 1024);
  assert(proctbl[i] == NULL);
  proctbl[i] = (Process *)alloc_page(1);
  for (int j = 127; j >= 0; --j) {
    proctbl[i][j].parent = freeproc;
    freeproc = &proctbl[i][j];
  }
}


void arch_proctable_init()
{
  proctbl = (Process **)alloc_page(1);
  for (int i = 0; i < 1024; ++i) {
    proctbl[i] = NULL;
  }
  alloc_procframe(0);
}


Process *arch_get_process(int pid)
{
  if ((pid < 0) || (pid > 0x1FFFF))
    return NULL;
  else {
    int lower = pid & 0x7F;
    int upper = (pid >> 7) & 0x3FF;
    return proctbl[upper] + lower;
  }
}

Process *arch_get_open_proc()
{
  Process *result;
  if (!freeproc) {
    for (int i = 0; i < 1024; ++i) {
      if (!proctbl[i]) {
        alloc_procframe(i);
        break;
      }
    }
  }
  
  result = freeproc;
  freeproc = freeproc->parent;
  return result;
}

void arch_add_open_proc(Process *p)
{
  p->parent = freeproc;
  freeproc = p;
}
