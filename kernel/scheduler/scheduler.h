#ifndef __SCHEDULER_H__
#define __SCHEDULER_H__

#include <stdint.h>
#include <vmem.h>
#include <context.h>

typedef enum ProcessPerms {
  PROCESSPERM_KERNEL = 0x01,
} ProcessPerms;

typedef struct Process {
  int prio;
  int status;
  uint32_t perms;
  PagingData *pdata;  /* paging data, part of vmem module */
  ContextData *context; /* context switching data */
  struct Process *parent;
  int32_t _unused;
  int32_t __unused;
} Process;

unsigned int sched_getsystick();

/* update the scheduler's internal tick. */
void sched_tick();

void scheduler_init();

/* get the currently running process */
Process *sched_get_current_process();

/* set the currently running process internal variable.
 platform implementation has to do the actual context switch. */
void sched_set_current_process(Process *proc);

/* schedule a new process and put it into waiting state.
   if there is a currently-running process, the new
   process will be presumed to be the currently-running
   process' child. If there is no currently-running process,
   an error is presumed, the Process is not added to the
   waiting queue, and -1 is returned.
   returns the PID of the new process if successful.
   
   The Process struct pointed to by proc
   should just be filled with zeros. */
int sched_new_process();

/* adds a new ring 3 process.
  entrypoint is a physical address and MUST be page-aligned. */
int sched_new_kernel_process(
  int prio,
  uint32_t perms,
  uint32_t *entrypoint);

/* get a pointer to a Process from its pid. */
Process *sched_get_process(int pid);

/* jump into ring3. This should be handed the pointer to the
 idle process, which should be the only process created at 
 time of call. It should not have been put in the ready queue. */
void sched_start(Process *proc);

#endif /* __SCHEDULER_H__ */
