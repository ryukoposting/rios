/* Scheduler's ready-queue */

/* #include <stdlib.h> */
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include "scheduler.h"
#include "readyqueue.h"

typedef struct SchNode {
  int p;
  Process *proc;
} SchNode;


/* `readyqueue` is a min-heap implemented as a circular buffer
   `schalloc` is the number of SchNode-sized elements 
              allocated to `readyqueue`
   `schlen` is the number of elements in the heap
   `start` if schlen > 0: the first index currently in use in
           the circular buffer. if schlen == 0: start=0
   `tail` if schalloc > schlen > 0: the first unused element
          in the circular buffer. if schlen==schalloc,
          tail==start. if schlen == 0: tail=1*/
static SchNode *readyqueue = NULL;
static int schalloc = 0;
static int start = 0, tail = 1, schlen = 0;


__attribute__((unused)) static int ceil8(int n)
{
  int result = (n * 2) & ~(0x7);
  if (result <= n)
    result += 8;
  assert(result > n);
  return result;
}


static void readyqueue_swapreal(int i, int j)
{  /* swap the elements at two indices in `readyqueue` */
  int temp_p;
  Process *temp_proc;
  temp_p = readyqueue[i].p;
  temp_proc = readyqueue[i].proc;
  readyqueue[i].p = readyqueue[j].p;
  readyqueue[i].proc = readyqueue[j].proc;
  readyqueue[j].p = temp_p;
  readyqueue[j].proc = temp_proc;
}


static int readyqueue_alloc(int len)
{
  int result = schalloc;
  SchNode *newptr;
  if (len > schalloc) {
    result = ceil8(len);
    int byte_len = sizeof(SchNode) * result;
    if (!readyqueue) {
      newptr = malloc(byte_len);
      memset(newptr, 0, byte_len);
      start = 0;
      tail = 1;
    } else {
      newptr = malloc(byte_len);
      memset(newptr, 0, byte_len);
      memcpy(newptr, readyqueue + start, (schalloc - start) * sizeof(SchNode));
      if (start > 0)
        memcpy(newptr + start, readyqueue, tail * sizeof(SchNode));
      free(readyqueue);
      start = 0;
      tail = schlen;
    }
    readyqueue = newptr;
    schalloc = result;
  }
  return result;
}


static int sindex(int i)
{
  return (i + start) % schalloc;
}


static void readyqueue_put(Process *proc)
{ /* adds to readyqueue without properly ordering it */
  int p = sched_getsystick() + proc->prio;
  readyqueue_alloc(schlen + 1);
  if (schlen > 0) {
    readyqueue[tail].p = p;
    readyqueue[tail].proc = proc;
    tail = (tail + 1) % schalloc;
  } else {
    readyqueue[start].p = p;
    readyqueue[start].proc = proc;
    tail = (start + 1) % schalloc;
  }
  
  schlen += 1;
}


static void readyqueue_arrangeup(int i)
{ /* rearrange readyqueue in min-heap order starting from
     position i (i is not the real index, but the position
     in the heap) and working upwards */
  int parent, si, sp;
  while ((schlen > 1) && (i > 0)) {
    parent = (i - 1) / 2;
    si = sindex(i);
    sp = sindex(parent);
    if (readyqueue[si].p < readyqueue[sp].p) {
      readyqueue_swapreal(si, sp);
    }
    i = parent;
  }
}


__attribute__((unused)) static void readyqueue_heapify(int i)
{ /* min-heapify starting from position i (i is not the real
     index, but the position in the heap) and work downwards */
  int l = (i * 2) + 1;
  int r = (i * 2) + 2;
  int sl = sindex(l);
  int sr = sindex(r);
  int small = i;
  if ((l < schlen) && (readyqueue[sl].p < readyqueue[sindex(small)].p))
    small = l;
  if ((r < schlen) && (readyqueue[sr].p < readyqueue[sindex(small)].p))
    small = r;
  if (small != i) {
    readyqueue_swapreal(sindex(i), sindex(small));
    readyqueue_heapify(small);
  }
}


Process *readyqueue_pop()
{
  Process *result = NULL;
  if (schlen > 0) {
    result = readyqueue[start].proc;
    start = (start + 1) % schalloc;
    schlen -= 1;
  }
  /* readyqueue_heapify(0); */
  return result;
}


void readyqueue_ontickrollover()
{
  if (schlen > 0) {
    for (int i = 0; i < schlen; ++i)
      readyqueue[sindex(i)].p = readyqueue[sindex(i)].proc->prio;
  }
}


void readyqueue_add(Process *proc)
{
  readyqueue_put(proc);
  readyqueue_arrangeup(schlen - 1);
}
