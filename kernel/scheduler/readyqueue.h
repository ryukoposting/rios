#ifndef __READYQUEUE_H__
#define __READYQUEUE_H__

void readyqueue_ontickrollover();

Process *readyqueue_pop();

void readyqueue_add(Process *proc);

#endif /* __READYQUEUE_H__ */
