#ifndef __CONTEXT_H__
#define __CONTEXT_H__

typedef struct ContextData ContextData;
#ifdef BUILDING_SCHEDULER
#include <arch_context.h>
#endif



ContextData *context_new_contextdata();

#endif
