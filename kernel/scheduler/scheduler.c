#include <limits.h>
#include <assert.h>
#include "scheduler.h"
#include "readyqueue.h"
#include <arch_scheduler.h>
#include <arch_context.h>

static unsigned int systick;
static Process *current;

void scheduler_init()
{
  systick = 0;
  current = NULL;
  arch_proctable_init();
}

unsigned int sched_getsystick()
{
  return systick;
}

void sched_tick()
{
  if (systick < INT_MAX){
    systick += 1;
  } else {
    systick = 0;
    readyqueue_ontickrollover();
  }
}

Process *sched_get_process(int pid)
{
  return arch_get_process(pid);
}

int sched_new_process()
{
  int result = -1;
  if (current) {
    Process *np = arch_get_open_proc();
    assert(np);
    result = arch_get_pid(np);
    np->parent = current;
    np->prio = np->parent->prio;
    np->status = 0;
    np->perms = np->parent->perms;
    np->pdata = vmem_new_pagingdata();
    np->context = context_new_contextdata();
  }
  return result;
}

int sched_new_kernel_process(
  int prio,
  uint32_t perms,
  uint32_t *entrypoint)
{
  int result;
  Process *np = arch_get_open_proc();
  assert(np);
  result = arch_get_pid(np);
  np->prio = prio;
  np->status = 0;
  np->perms = perms;
  np->parent = NULL;
  np->pdata = vmem_new_pagingdata();
  vmem_create_mapping(np->pdata,
    0x400000,
    PAGING_ENTRY_FLAG_RING3 | PAGING_ENTRY_FLAG_WRITABLE);
  np->context = context_new_contextdata();
  np->context->tss.esp = (0x400000 + 4096) - 16;
  np->context->tss.ebp = np->context->tss.esp;
  np->context->tss.eip = entrypoint;
  return result;
}
