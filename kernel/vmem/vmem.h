#ifndef __VMEM_H__
#define __VMEM_H__

/* top-level vmem stuff is platform-agnostic. That's why
 the main structure is called "PagingData" instead of
 "page directory." */

#include <stddef.h>

/* implementation of struct PagingData is in arch_vmem.h */
typedef struct PagingData PagingData;

#ifdef BUILDING_VMEM
#include <arch_vmem.h>
#endif

typedef enum PagingEntryFlags {
  PAGING_ENTRY_FLAG_RING3 = 0x4,
  PAGING_ENTRY_FLAG_WRITABLE = 0x2
} PagingEntryFlags;

void vmem_initialize();

PagingData *vmem_new_pagingdata();

/* vaddr and paddr are assumed to be byte aligned */
void vmem_set_mapping(
  PagingData *pd,
  void *vaddr,
  void *paddr,
  unsigned int flags);

/* automatically map a page */
void vmem_create_mapping(PagingData *pd, void *vaddr, unsigned int flags);

/* set the flags for an already-mapped page */
void vmem_set_flags(PagingData *pd, void *vaddr, unsigned int flags);

/* get kernel paging data */
PagingData *vmem_get_kpaging();

extern void vmem_load_pagingdata(PagingData *pd);

extern void vmem_enable_paging();

#endif
