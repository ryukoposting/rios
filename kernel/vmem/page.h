#ifndef __VMEM_PAGE_H__
#define __VMEM_PAGE_H__
#include <stdint.h>
#include <stddef.h>

void *alloc_page(size_t npages);

#endif
