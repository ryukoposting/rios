#include <page.h>
#include <multiboot.h>
#include <assert.h>
#include <stdio.h>

// TODO: static assert size=4096
typedef struct Page {
  uint32_t size;
  struct Page *next;
  uint32_t pad[1022];
} Page;

static Page *free_pool = NULL;
static Page *try_alloc_from_pool(size_t npages)
{
  Page *result = NULL;
  if (free_pool) {
    Page *p = free_pool->next;
    Page *pprev = free_pool;
    do {
      if (p->size > npages) {
        result = p;
        pprev->next += npages;
        pprev->next->next = p->next;
        pprev->next->size = p->size - npages;
        free_pool = pprev->next;
        break;
      } else if (p->size == npages) {
        result = p;
        pprev->next = p->next;
        if (p == pprev) {
          free_pool = NULL;
        } else
          free_pool = pprev;
        break;
      }
      pprev = p;
      p = p->next;
    } while (p != free_pool->next);
  }
  return result;
}

static Page *free_page(Page *p)
{
  p->size = 1;
  p->next = p;
  
  if (free_pool) {
    Page *pp;
    for (pp = free_pool; ; p = p->next) {
      if ((pp <= p) && (pp->next >= p))
        break;
      if ((pp >= pp->next) && ((pp < p) || (pp->next > p)))
        break;
    }
    
    if (p + p->size == pp->next) {
      p->size += pp->next->size;
      p->next = pp->next->next;
    } else {
      p->next = pp->next;
    }
    
    if (pp + pp->size == p) {
      pp->size += p->size;
      pp->next = p->next;
    } else {
      pp->next = p;
    }
  }
  
  free_pool = p;
}


void *alloc_page(size_t npages)
{
  Page *result = try_alloc_from_pool(npages);
  
  while (!result) {
    //puts("retrieving multiboot frame\n");
    free_page((Page *)multiboot_frame_allocate());
    result = try_alloc_from_pool(npages);
  }
  
  return result;
}
