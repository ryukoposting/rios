#ifndef __ARCH_VMEM_H__
#define __ARCH_VMEM_H__

#include <stddef.h>
#include <stdint.h>

typedef struct PageTable {
  uint32_t table[1024];
} __attribute__((aligned(4096))) PageTable;

struct PagingData {
  uint32_t directory[1024];
} __attribute__((aligned(4096)));


#endif
