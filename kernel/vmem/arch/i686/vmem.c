#include <vmem.h>
#include <page.h>
#include "arch_vmem.h"

#include <multiboot.h>
#include <stdio.h>
#include <assert.h>

static inline PageTable *get_tbl(PagingData *pd, int i)
{
  assert(pd->directory[i] & 1);
  return pd->directory[i] & (~0xFFF);
}


PagingData *vmem_new_pagingdata()
{
  PagingData *result = (PagingData *)alloc_page(1);
  for (int i = 0; i < 1024; ++i)
    /* set each entry as not present, ring-3 accessible, writable */
    result->directory[i] = PAGING_ENTRY_FLAG_WRITABLE | PAGING_ENTRY_FLAG_RING3;
  
  for (size_t i = 0; i < 0x400000; i += 0x1000)
    vmem_set_mapping(result, (void *)i, (void *)i,
      PAGING_ENTRY_FLAG_WRITABLE | PAGING_ENTRY_FLAG_RING3);
  
  return result;
}

void vmem_set_mapping(
  PagingData *pd,
  void *vaddr,
  void *paddr,
  unsigned int flags)
{
  int dir = ((uint32_t)vaddr & 0xFFC00000) >> 22;
  int tbl = ((uint32_t)vaddr & 0x003FF000) >> 12;
  flags &= 0xFFF;
  
  // OR'ing in 1 signifies that this page has been allocated
  uint32_t entry = ((uint32_t)paddr & ~0xFFF) | flags | 1;
  
  if (!(pd->directory[dir] & 0x1)) {
    uint32_t x = (uint32_t)alloc_page(1);
    assert(x);
    x |= (flags & PAGING_ENTRY_FLAG_RING3) | 3;
    pd->directory[dir] = x;
  }
  
  get_tbl(pd, dir)->table[tbl] = entry;
}

void vmem_create_mapping(PagingData *pd, void *vaddr, unsigned int flags)
{
  vmem_set_mapping(pd, vaddr, alloc_page(1), flags);
}
  

int32_t vmem_get_mapping(PagingData *pd, void *vaddr)
{
  int32_t result = -1;
  int dir = ((uint32_t)vaddr & 0xFFC00000) >> 22;
  int tbl = ((uint32_t)vaddr & 0x003FF000) >> 12;
  int offset = ((uint32_t)vaddr & 0xFFF);
  
  if (pd->directory[dir] & 1) {
    if (get_tbl(pd, dir)->table[tbl] & 1) {
      result =
        (get_tbl(pd, dir)->table[tbl] & (~0xFFF)) | offset;
    }
  }
  
  return result;
}

void vmem_set_flags(PagingData *pd, void *vaddr, unsigned int flags)
{
  int dir = ((uint32_t)vaddr & 0xFFC00000) >> 22;
  int tbl = ((uint32_t)vaddr & 0x003FF000) >> 12;
  uint32_t entry = get_tbl(pd, dir)->table[tbl];
  entry = (entry & ~0xFFE) | (flags & 0xFFE); // E keeps "present" bit
  get_tbl(pd, dir)->table[tbl] = entry;
}
