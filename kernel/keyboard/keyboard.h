#ifndef __KEYBOARD_H__
#define __KEYBOARD_H__

#include <stddef.h>
#include <stdint.h>

//TODO: add locale configuration for different sets of keycodes

#include "arch/i686/keycodes.h"

typedef enum KeyCodes_1 KeyCode;

typedef struct KeyData {
  int printable : 1;
  int alpha : 1;
  int alphanum : 1;
  int capslk : 1;
  int scrollk : 1;
  int numlk : 1;
  int shift : 1;
  int ctrl : 1;
  int alt : 1;
  int gui : 1;  // aka windows button
  int app : 1;  // aka menu button
  int press : 1;
  KeyCode keycode;
  char asciirepr;
} KeyData;

void kbd_init();

void kbd_setcodeset(int kcodeset);

int kbd_getcodeset();

/* returns 1 if there are additional bytes to be read
 for this keycode. returns 0 if the keycode is complete.
 returns -1 if the keycode is not valid for the current
 codeset. 'keycode' points to an array of bytes received
 from the keyboard, ordered first to last. */
int kbd_continuation(uint8_t *keycode, int len);

int kbd_handle(KeyData *out, uint8_t *keycode, int len);

#if defined(BUILDING_KEYBOARD)
#include "arch_keyboard.h"
#endif /* defined(BUILhDING_KEYBOARD) */

#endif
