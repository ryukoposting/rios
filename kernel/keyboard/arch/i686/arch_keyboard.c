#include "keyboard.h"
#include <string.h>
#include <assert.h>

uint8_t keystates[KC1_LEN];

int kbd_continuation(uint8_t *keycode, int len)
{
  assert(len > 0);
  int result = -1;
  
  if (kbd_getcodeset() == 1) {
    switch (len) {
    case 1:
      result = (keycode[0] == 0xE0) || (keycode[0] == 0xE1);
      break;
    case 2:
    case 3:
      result = (keycode[0] == 0xE1) ||
        (keycode[1] == kcset_1[KC1_PSPR + 1]) ||
        (keycode[1] == kcset_1[KC1_PSRE + 1]);
      break;
    case 4:
    case 5:
      result = (keycode[0] == 0xE1);
      break;
    default:
      result = 0;
    }
  }
  
  return result;
}

int arch_kbd_handle(struct KeyData *out, uint8_t *keycode, int len)
{
  assert(out);
  assert(keycode);
  int result = -1;
  uint8_t k;
  
  memset(out, 0, sizeof(KeyData));
  
  if (kbd_getcodeset() == 1) {
    switch (len) {
    case 1:
      k = keycode[0] & 0x7F;
      for (size_t i = KC1_ESC; (i <= KC1_F12) && result; ++i) {
        if (k == kcset_1[i]) {
          result = 0;
          out->press = ((keycode[0] & 0x80) == 0);
          out->keycode = i;
        }
      }
      break;
    case 2:
      k = keycode[1] & 0x7F;
      for (size_t i = KC1_PREVTRACK; (i <= KC1_MEDIASEL) && result; ++i) {
        if (k == kcset_1[i]) {
          result = 0;
          out->alphanum = 0;
          out->press = ((keycode[1] & 0x80) == 0);
          out->keycode = i;
        }
      }
      break;
    case 4:
      if (!memcmp(kcset_1 + KC1_PSPR, keycode, 4)) {
        out->alphanum = 0;
        out->printable = 0;
        out->press = 1;
        out->keycode = KC1_PSPR;
      } else if (!memcmp(kcset_1 + KC1_PSRE, keycode, 4)) {
        out->alphanum = 0;
        out->printable = 0;
        out->press = 0;
        out->keycode = KC1_PSRE;
      }
      break;
    }
  }
  
  return result;
}
