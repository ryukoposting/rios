#ifndef __ARCH_KEYBOARD_H__
#define __ARCH_KEYBOARD_H__

#include "keycodes.h"
#include <stddef.h>
#include <stdint.h>
#include "keyboard.h"

int arch_kbd_handle(struct KeyData *out, uint8_t *keycode, int len);

#endif
