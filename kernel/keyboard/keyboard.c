#include "keyboard.h"
#include "keycodes.h"

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

static int codeset;

void kbd_init()
{
  codeset = 1;
  
  uint8_t readb;
  int has_second_channel = 0;
  outb(0x64, 0x20);
  while (!(inb(0x64) & 1));
  readb = inb(0x60);
  puthex(readb);
  readb |= 1;  // enable first PS/2 interrupt
  outb(0x64, 0x60);
  while (inb(0x64) & 2);
  outb(0x60, readb);
  while (inb(0x64) & 2);
  outb(0x64, 0xAE); // enable the first PS/2 port
  while (inb(0x64) & 2);
}

void kbd_setcodeset(int kcodeset)
{
  assert(kcodeset > 0);
  codeset = kcodeset;
}

int kbd_getcodeset()
{
  return codeset;
}

int kbd_handle(KeyData *out, uint8_t *keycode, int len)
{
  int result = arch_kbd_handle(out, keycode, len);
  if (!result) {
    switch (out->keycode) {
    case KC1_CAPSLK:
    case KC1_NUMLK:
    case KC1_SCRLLK:
      if (out->press)
        keystates[out->keycode] = !keystates[out->keycode];
      break;
    default:
      keystates[out->keycode] = (out->press != 0);
    }
    
    out->capslk = keystates[KC1_CAPSLK] == 1;
    out->scrollk = keystates[KC1_SCRLLK] == 1;
    out->numlk = keystates[KC1_NUMLK] == 1;
    out->shift = keystates[KC1_LSHIFT] || keystates[KC1_RSHIFT];
    out->ctrl = keystates[KC1_LCTRL] || keystates[KC1_RCTRL];
    out->alt = keystates[KC1_LALT] || keystates[KC1_RALT];
    out->gui = keystates[KC1_LGUI] || keystates[KC1_RGUI];
    out->app = keystates[KC1_APPS] || keystates[KC1_RGUI];
    
    out->alpha =
      ((out->keycode >= KC1_Q) && (out->keycode <= KC1_P)) ||
      ((out->keycode >= KC1_A) && (out->keycode <= KC1_L)) ||
      ((out->keycode >= KC1_Z) && (out->keycode <= KC1_M));
    out->alphanum = out->alpha ||
      ((out->keycode >= KC1_1) && (out->keycode <= KC1_0));
      //TODO: add keypad wout->keycodeth check for numlock
    out->printable = out->alphanum ||
      ((out->keycode >= KC1_HYPH) && (out->keycode <= KC1_EQU)) ||
      ((out->keycode >= KC1_TAB) && (out->keycode <= KC1_ENTER)) ||
      ((out->keycode >= KC1_SEMCOL) && (out->keycode <= KC1_BTICK)) ||
      ((out->keycode >= KC1_BSLASH) && (out->keycode <= KC1_FSLASH)) ||
      (out->keycode == KC1_KPSTAR) || (out->keycode == KC1_SPACE) ||
      (out->keycode == KC1_KPHYPH) || (out->keycode == KC1_KPPLUS) ||
      (out->keycode == KC1_KPPERD) || (out->keycode == KC1_KPENTER) ||
      (out->keycode == KC1_KPSLASH);
    
    if (out->alpha) {
      if (out->capslk ^ out->shift)
        out->asciirepr = kcset_1_ascii_shift[out->keycode];
      else
        out->asciirepr = kcset_1_ascii[out->keycode];
    } else if (out->printable) {
      if (out->shift)
        out->asciirepr = kcset_1_ascii_shift[out->keycode];
      else
        out->asciirepr = kcset_1_ascii[out->keycode];
    } else if (out->keycode == KC1_BACKSP) {
      out->asciirepr = kcset_1_ascii[out->keycode];
    }
  }
  return result;
}
