#ifndef __NODES_H__
#define __NODES_H__

#include <stddef.h>
#include <stdint.h>
#include <assert.h>

typedef struct Node Node;

struct Node {
  uint8_t media_id;
  uint8_t protocol_rev;
  uint8_t n_devices;
  uint8_t block_size_base;
  uint8_t blocks_per_group_base;
  uint8_t block_size_offset;
  uint8_t blocks_per_group_offset;
  uint8_t nodes_per_block;
  uint32_t n_alloc_blocks;
  uint32_t n_blocks;
  uint64_t last_consistency_check;
  uint32_t consistency_check_interval;
  uint16_t tags_per_node;
  uint32_t block_offset;
  uint16_t magic_num;
};

#endif
