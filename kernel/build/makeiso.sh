#!/bin/bash

if [ ! -f "kernel.bin" ]; then
  echo "kernel.bin not found. did you run buildarch.sh?"
  exit 1
fi

mkdir -p iso/boot/grub
cp kernel.bin iso/boot/grub/kernel.bin
cp grub.cfg iso/boot/grub/grub.cfg
grub-mkrescue -o rios.iso iso

exit 0
