#!/bin/bash

source ./localconfig

module_stat_sem="./.buildmodule_stat"
echo "0" > "$module_stat_sem"

# duplicate the line below for each module, putting & between
# them to run in parallel
./buildmodule.sh scheduler "$module_stat_sem" || touch .buildmodfail &
./buildmodule.sh klibc "$module_stat_sem" || touch .buildmodfail &
for job in `jobs -p`
do
  wait $job
done


test -f .buildmodfail
if [ $? -eq 0 ]; then
  rm -f "./.buildmodfail"
  rm "./.buildmodule_stat"
  exit 1
else
  rm "./.buildmodule_stat"
  exit 0
fi
