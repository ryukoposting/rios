#!/bin/bash

# cleanmodule.sh [modulename...]
# cleans the specified module of all binaries
# and checksum files.

for module in "$@"
do
  echo "$module" | grep -q "[ \\/().=]"
  if [ $? -eq 1 ]; then
    rm -f ../$module/*.o ../$module/*.a ../$module/checksums
  fi
done
