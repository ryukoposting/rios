#!/bin/bash

# buildmodule.sh [modulename] [statfile]
# builds one OS module.
# before each call to the compiler, statfile is checked.
# if statfile contains 0, the build continues.
# if statfile contains something else, something is assumed to
# have failed, and the build cancels.

echo "$1" | grep -q "[ \\/().=]"
if [ $? -eq 0 ]; then
  echo "buildmodule: invalid arg1"
  exit -1
fi

echo "$2" | grep -q "[ \\v()=]"
if [ $? -eq 0 ]; then
  echo "buildmodule: invalid arg2"
  exit -1
fi

statfile=$2
modulename="$1"
moduledir="../$1"

if [ ! -d $moduledir ]; then
  echo "module '$1' does not exist."
  exit 1
fi

moduleheaders=$(find "$moduledir" -type f -name *.h)
modulesources=$(find "$moduledir" -type f | grep -E "\\.(c|s)$")


function onexit() {
  rm -f "$moduledir/.checksums"
}

function build_file() {
  #pushd "$moduledir" >> /dev/null
  # $GCC $CFLAGS $LOCAL_CFLAGS -c "$1" -o $1.o
  make -s -C$moduledir $(printf "$1" | sed "s,^$moduledir/,,")
  local r=$?
  #popd >> /dev/null
  return $r
}

# rebuilds changed source files. if a header file changed,
# rebuilds all files.
function fix_differences() {
  changed_files=""
  local old_sums=""
  local is_dead="0"
  local rebuild_all="0"
  
  rm -f "$moduledir/.checksums"
  touch "$moduledir/.checksums"
  if [ -f "$moduledir/checksums" ]; then
    #echo "loading checksums"
    local old_sums=$(cat "$moduledir/checksums")
  fi
  
  # check for changes in header files.
  # if any header files changed, we will rebuild all source files.
  while read line ; do
    local newsum=$(md5sum "$line" | tr -d '\n' | sed 's/  / /g')
    local currentstat=$(cat "$statfile" | tr -d '\n')
    echo "$old_sums" | grep -Fxq "$newsum"
    if [ "$?" -ne "0" ]; then
      rebuild_all="1"
    fi
    echo $newsum >> "$moduledir/.checksums"
  done <<<$(echo "$moduleheaders")
  
  # build source files.
  while read line ; do
    local newsum=$(md5sum "$line" | tr -d '\n' | sed 's/  / /g')
    local currentstat=$(cat "$statfile" | tr -d '\n')
    echo "$old_sums" | grep -Fxq "$newsum"
    local chksum_match=$?
    if [ "$rebuild_all" -eq "0" ] && [ "$chksum_match" -eq "0" ]; then
      # old checksum matches current, don't build
      echo $newsum >> "$moduledir/.checksums"
      #echo "not rebuilding $line: checksum matches"
    else
      # need to rebuild this file
      if [ "$is_dead" -eq "0" ] && [ "$currentstat" -eq "0" ]; then
        # previous builds have all been okay, so do this one
        changed_files=$(printf "$changed_files $line")
        echo "rebuild $line"
        build_file $line
        is_dead=$?
        # if this build was okay, add it to the new checksums
        # otherwise, set the semaphore file to indicate that an
        # error occurred
        if [ "$is_dead" -eq "0" ]; then
          echo $newsum >> "$moduledir/.checksums"
        else
          echo "1" > "$statfile"
        fi
      else
        echo "not rebuilding $line: previous error"
      fi
    fi
  done <<<$(echo "$modulesources")
  return "$is_dead"
}


function link_objects() {
  #local objectfile_names=$(echo "$changed_files" | sed "s/\\.c/.c.o/g")
  local objectfile_names=$(echo $modulesources | sed "s/\\.c/.c.o/g")
  #echo "linking $modulename..."
  #$AR rcs "$moduledir/$modulename.a" $objectfile_names
  make -s -C$moduledir staticlib
  return $?
}



trap onexit EXIT


fix_differences
result=$?

if [ $result -eq 0 ]; then
  # all source file builds succeeded, update checksum file
  mv "$moduledir/.checksums" "$moduledir/checksums"
  if [ ! -z "$changed_files" ]; then
    # some source files changed, need to re-link
    link_objects
    result=$?
  fi
else
  echo "$modulename: failed to build module"
fi


exit $result
