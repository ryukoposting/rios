#!/bin/bash

function finish {
  mv mnt/os/kernel/Makefile.config.bak mnt/os/kernel/Makefile.config
}
trap finish EXIT

sed -i.bak -e 's,export PATH.*,export PATH:=/opt/cross/bin:/opt/cross:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin,g' mnt/os/kernel/Makefile.config

sed -i -e 's,export SYSROOT.*,export SYSROOT:="/opt/cross"",g' mnt/os/kernel/Makefile.config

# make -Cmnt/os/kernel/build clean
make -Cmnt/os/kernel/build kernel

mkdir -p mnt/os/kernel/build/isodir/boot/grub
cp mnt/os/kernel/build/kernel.bin mnt/os/kernel/build/isodir/boot/kernel.bin
cp mnt/os/kernel/build/grub.cfg mnt/os/kernel/build/isodir/boot/grub/grub.cfg
grub-mkrescue -o mnt/os/kernel/build/kernel.iso mnt/os/kernel/build/isodir
