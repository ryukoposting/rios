\documentclass[sigconf]{acmart}

\usepackage{booktabs} % For formal tables
\usepackage{multicol}
\usepackage{verbatim}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{float}
\usepackage{xcolor}
\usepackage{soul}
\usepackage{lipsum}
\usepackage{amsmath}
%\usepackage{draftwatermark}
%\SetWatermarkText{DRAFT}
%\SetWatermarkScale{2}
\graphicspath{ {../images/} }

\acmConference[]{This isn't Actually an ACM Paper}{December 2019}{St. Paul, Minnesota, USA}
\acmYear{2019}
\copyrightyear{2019}
\acmDOI{aa.aaa/aaa_a}
\acmISBN{aaa-aaaa-aa-aaaa/aa/aa}

\begin{document}
\title{A Homogeneous, Distributed System for Indexing and Storing Persistent Data }
%\titlenote{Produces the permission block, and copyright information}
%\subtitle{Extended Abstract}
%\subtitlenote{The full version of the author's guide is available as \texttt{acmart.pdf} document}

\author{Evan Perry Grove}
\affiliation{%
    \city{St. Paul}
    \state{Minnesota}
    \postcode{55118}
}
\email{epg@ryuk.ooo}


% The default list of authors is too long for headers.
%\renewcommand{\shortauthors}{More Authors et. al.}

\begin{abstract}

put the abstract here

\end{abstract}

% Can turn this off if necessary
\keywords{}


%%%%%%%%%%%%%%% BEGIN PAPER %%%%%%%%%%%%%%%
\maketitle

%\input{samplebody-conf} % Formatting examples in here

\section{Introduction}
\label{sec:intro}

For decades, system-level management of persistent storage has fixated on tree-like structuring. The use of "file systems" as the structuring mechanism for persistent system information is the best-known manifestation of this. While the use of trees to organize persistent data allows for easy optimizations to indexing algorithms, many kinds of data often stored on a personal or mobile computer do not exhibit tree-like categorizaion. As a result, organization of information in file systems becomes messy and unintuitive.

Some attempts have been made to design a more intuitive system for structuring persistent computer data. In a tagging file system, files can be assigned an arbitrary number of categories. These categories, represented by "tags," allow for pieces of information to be placed into multiple disjoint sets, instead of being placed within a series of nested sets. while a well-intentioned step away from tree-based categorization of data, do not destroy the aforementioned false dichotomy between information, and categorizations thereof.

However, tagging file systems are not a complete solution. Like a traditional file system, tagging file systems enforce a false dichotomy between information, and categorizations thereof. That is, the intuitive difference between files and folders is a construct of the file system, and is not representative of the way information is naturally organized. This contributes to semantic confusion when organizing files. An example-based argument for this is made in Section \ref{ssec:motivation}.

Most file systems also assume that all data is stored on local persistent storage. As a result, systems for indexing information not stored on a local machine have evolved independently from file systems. Examples of this include cloud file storage services like Dropbox, Google Drive, and Microsoft OneDrive. These services require their own client applications to index and manipulate data on remote computers, despite the fact that there is no inherent difference between the information one stores on a remote server, and on a local computer. These myriad applications create unnecessary inconvenience for computer users.

Mechanisms for presenting local and remote information homogeneously exist, but have not seen wide adoption. Distributed operating systems such as Plan 9 rely on communication protocols that represent information about remote files in the same manner as locally-stored information. However, these operating systems still use file systems that enforce the false dichotomy between data and its categorizations.

In this paper, I propose a Homogeneous, Distributed Persistent Storage (HDPS) system. In HDPS, persistent blocks of information (known as \textit{nodes}) may be categorized by any other node. This categorization is known as \textit{tagging}. A node may use any other node, locally or remotely stored, as a tag. Nodes may use an arbitrary number of other nodes as tags, and an arbitrary number of nodes may use any single node as a tag. This provides an intuitive representation of discrete pieces of information, and the relationships between them. 

\subsection{Motivation: A Man and His Music}
\label{ssec:motivation}

\begin{figure}
    \centering
    \includegraphics[width=\columnwidth]{musicgraph.png}
    \caption{Illustration of the "Man and his Music" example.}
    \label{fig:musicgraph}
\end{figure}

Consider this scenario, which will be referenced throughout the rest of this document. One wishes to organize their collection of digital music. This individual also has files containing biographies of various musicians, descriptions of different music albums, and descriptions of various nations. All of this data is interrelated. A directed graph illustrating this scenario is shown in figure \ref{fig:musicgraph}.

Each of the songs was recorded for an album, and was made by one or more artists, who may be a band or individual musician. The musicians each have a nation of origin, which is not necessarily the same as the nation in which the album was recorded. Some songs have multiple artists, and songs may also appear on multiple albums. Some songs are attributed to a different artist than the albums on which those songs appear. Some albums were recorded in multiple countries. Cycles exist between musicians and the bands they led.

The simple observation that the illustration in figure \ref{fig:musicgraph} is not a tree is enough to prove that it is not possible to represent all data relationships in this graph using a single tree. In general, the practical result of this is that arbitrary data in a file system can only be organized by a small subset of the relationships it may have with other data.

\section{Design}
\label{sec:design}

\begin{figure}
    \centering
    \includegraphics[width=\columnwidth]{mathgraph.png}
    \caption{A directed graph.}
    \label{fig:mathgraph}
\end{figure}

\subsection{Mathematical Basis}
\label{ssec:math}
A set of discrete pieces of information and relationships between it may be represented by a directed graph

  \[G=(V, E)\]

$V$ is the set of all vertices in the graph. $E$ is the set of all edges, represented as ordered pairs. Here, an ordered pair $(a, b) \in E$ shall represent a relationship from $a$ to $b$. For example, for the graph in Figure \ref{fig:mathgraph}, 
  \[V=\{a, b, c, d, e, f, g, h, i\}\]
\begin{center}
\begin{align*}
  E=\{&(d, a), (d, b), (e, a), (e, b), (e, f), (f, c), (f, e),\\
    &(g, b), (g, d), (g, e), (h, b), (h, e), (h, f), (i, e), (i, f)\}
\end{align*}
\end{center}

A collection of nodes must be able to represent discrete pieces of data, where one piece of information is mapped to a single node. In order for the design of a node to be valid, it must be possible to implement the following operations on the node:

\begin{itemize}
  \item $FindTags : N \rightarrow \{N\}$: Given a node $n$, return all of $n$'s tags.
  \item $FindTagged : N \rightarrow \{N\}$: Given a node $n$, return all nodes that have $n$ as a tag.
\end{itemize}

Mathematical functions implementing these operations on the directed graph are trivial:

\[FindTags(v, E) = \{b \hspace{0.2cm} \forall \hspace{0.2cm} (v, b) \in E\}\]
\[FindTagged(v, E) = \{a \hspace{0.2cm} \forall \hspace{0.2cm} (a, v) \in E\}\]

Three designs for a node are introduced below. Each design is evaluated based on the storage spaced required to represent it, as well as the computational complexity of FindTags and FindTagged.

\subsubsection{A Space-Efficient Node Design}
\label{sssec:design1}

Given $v \in V$, let a node be defined as

\[(v, \{b \hspace{0.1cm} \forall \hspace{0.1cm} (v, b) \in E\})\]

To represent an entire graph $G=(V, E)$ with a collection of these nodes, $\mid V\mid$ nodes are needed. The total space required to represent a directed graph using a collection of these nodes is on the order of $V + E$, which is maximally efficient.

FindTags is trivial. Because the node contains a set of all of its tags, this operation requires $\Theta(1)$ time. However, FindTagged cannot be done efficiently. Because a node contains no information about what nodes use it as a tag, FindTagged for this node must search the set of all nodes, requiring $\Theta(\mid V \mid)$ time. In a practical implementation of HDPS, it must be assumed that there is a very large number of nodes, so this time requirement is prohibitive.

\subsubsection{A Time-Efficient Node Design}
\label{sssec:design2}

Given $v \in V$, let a node be defined as

\[(v, \{b \hspace{0.1cm} \forall \hspace{0.1cm} (v, b) \in E\}, \{a \hspace{0.1cm} \forall \hspace{0.1cm} (a, v) \in E\})\]

This node contains a set of all its tags, so FindTags requires time $\Theta(1)$, as in the previous design. Each node also contains a set of all nodes that use it as a tag, so FindTagged also runs in $\Theta(1)$ time. However, this design is not space efficient. Since each node contains all edges both entering and leaving its vertex, each edge in the graph is represented twice. Therefore, the total space required to represent a directed graph with this scheme is on the order of $V + 2E$. Asymptotically, this is the same as the maximally-efficient $V+E$ space. However, in practice, non-volatile storage is expensive, and nearly doubling the size of a node would be costly.

\subsubsection{A Space and Time-Efficient Node Design}
\label{sssec:design3}

Given $v \in V$, let a node be defined as

\[(v, \lambda(v), c)\]

Where $c$ is any vertex that has $v$ as a tag.

And the set of all nodes be represented by $\Lambda$. $\lambda$ is defined as follows:

\[U(t, v) = \{(t, b) \in E\} \setminus \{(t, v)\} \setminus \{n.\lambda \hspace{0.1cm} \forall\hspace{0.1cm} n \in \Lambda\}\]

\[
    W(t, v)=
\begin{cases}
    (t, v) & \text{if } \mid U(t, v) \mid = 0 \\
    (a, b) \in U(t, v) & \text{otherwise}
\end{cases}
\]

\[
\lambda(v) = \{W(t, v) \hspace{0.1cm}\forall\hspace{0.1cm} (t, v) \in E\}
\]


Observe that the definition of $U(t, v)$ is such that, when given a constant $t$ and some node $v$ that has $t$ as a tag, each distinct value of $v$ will return a different value from $U(t, v)$, and that $U(t, v)$ never returns $(t, v)$ unless it is the only edge going to $v$. As a result, if all vertices $k \in K$ have an outgoing edge to $t \notin K$, then the set $\{(k, U(t, k)) \hspace{0.1cm}\forall\hspace{0.1cm} k \in K\}$ forms a circular linked list. $\lambda(v)$ covers the special case where only a single node has a particular tag.

FindTags is completed by analyzing the first element of each pair in $\lambda(v)$, which is contained in the node corresponding to $v$. Therefore, FindTags requires $\Theta(1)$ time. For node $(v, \lambda(v), c)$, FindTagged finds all nodes tagged with $v$ by first traversing to $c$. The algorithm then finds finds the pair $(d, v) \in \lambda(c), (e, v) \in \lambda(d), (f, v) \in \lambda(e), ...$. The algorithm terminates when $(c, v) \in \lambda(k)$. As such, FindTagged's running time in this design $\Theta(\mid\{(u, v) \in E\}\mid)$, scaling linearly with the number of nodes using $v$ as a tag.

Intuitively, any discrete piece of information is not directly related to all other discrete pieces of information in the universe. As such, in the general case where HDPS nodes represent arbitrary information, it is reasonable to assume that the graphs represented by HDPS are sparse (that is, $\mid E\mid \ll \mid V\mid^2$). Since FindTagged for this design scales with the size of a subset of $E$, but scales with $\mid V\mid$ in the design presented in section \ref{sssec:design1}, it is reasonable to say that FindTagged will perform significantly better on average in the design presented here.

In this design, each node contains a vertex $v$, a set of edges $\lambda(v)$ with as many elements as there are edges leaving $v$, and a value $c$ that points to any vertex with an edge going into $v$, or is nil if no such vertex exists. Thus, the space to represent the entire directed graph with this node design is on the order of $2V+E$. In a graph where $\mid E\mid < \mid V\mid$, this is less efficient than the second design. However, in a directed graph where there is at least one edge leaving each vertex, $\mid E\mid \geqslant \mid V\mid$. In the context of HDPS, such a graph indicates that all nodes have at least one tag. In the implementation of HDPS in Section \ref{sec:impl}, it will become clear that all nodes always have at least one tag, so this is a more space-efficient design than that of Section \ref{sssec:design2}.


\section{Implementation}
\label{sec:impl}

The \textit{HDPS system} organizes memory on local storage volumes on a computer. An \textit{HDPS network} consists of multiple computers with HDPS systems which can transparently share stored information. HDPS differs from most file systems in that a single HDPS system can be spread across multiple storage devices on a single machine, and that the block level does not consider journaling, or user or group access rights. Unless otherwise stated, all values use little-endian byte order. (ADD MORE HERE)

\subsection{Media and Blocks}
\label{ssec:blocks}
When an operating system searches an HDPS system's storage devices, it will look for the \textit{superblock} on each media device. Up to 16 media devices may be used in a single HDPS system. The superblock is located at byte 1024 from the beginning of the device's storage area, and is always exactly 1024 bytes long.

\begin{center}
\begin{tabular}{ |c c|l| }
 \hline
 start byte & end byte & contents \\
 \hline
 0 & 0 & Media ID number \\
 1 & 1 & HDPS revision number (1) \\
 2 & 2 & Number of local storage devices \\
 3 & 3 & Block size (base) \\
 4 & 4 & Blocks per group (base) \\
 5 & 5 & Block size (offset) \\
 6 & 6 & Blocks per group (offset) \\
 7 & 7 & Nodes per block \\
 8 & 11 & Number of allocated blocks \\
 12 & 15 & Total number of blocks \\
 16 & 23 & Time of last consistency check \\
 24 & 27 & Max time between consistency checks \\
 28 & 29 & Max tags per node \\
 32 & 35 & Block offset \\
 56 & 57 & HDPS magic number 0xDD55 \\
 \hline
\end{tabular}
\end{center}

Note that the location of the superblock and the position of the magic number have been chosen specifically to overlap with the superblocks used by the ext file systems. The revision number in byte 1 is meant to allow for future modifications to HDPS.

Each storage device in an HDPS system is assigned a unique "media ID." The media ID allows HDPS to distinguish between references to memory on different storage volumes. As stated previously, up to 16 media devices are permitted in an HDPS system. The lowest media ID in any HDPS system is zero, and the highest media ID is one less than the value in byte 2.

The storage space in each media device is organized into block groups, which are then subdivided into blocks. The size of a media device's blocks is calculated by $(\textit{block size (base)}) \cdot 2^{(\textit{block size (offset)})}$. The number of blocks in each group is calculated by $(\textit{blocks per group (base)}) \cdot 2^{(\textit{blocks per group (offset)})}$. The maximum number of blocks allowed per group is $2^{20} = 1048576$, and up to $2^{20}=1048576$ block groups are allowed per media device. No general limit is defined for maximum block size besides the largest value that can be described by the calculation before. Blocks must be no less than 1024 bytes in size. For x86-based systems, a 4096-byte block size is strongly recommended.

The size of a node is calculated by $\frac{block size}{(\textit{nodes per block})}$. The maximum number of tags per node helps decide the structure of a node- this value will be referenced in Section \ref{ssec:nodes}. All media devices in an HDPS system must have nodes of the same size and structure. Node size should be no less than 2048 bytes, and all media devices in the HDPS system must specify the same byte offset for tags and blocks.

The time of last consistency check is represented as the number of seconds since 00:00:00 UTC on January 1, 1970. This is effectively a 64-bit extension of the traditional 32-bit Unix time. The time between consistency checks is represented in units of seconds.

The block offset contains the address of the first byte of the first block of block group 0 on this storage device.

Blocks may be \textit{unallocated}, \textit{node blocks}, or \textit{data blocks}. An unallocated block is one that is not in use, and may be used to create a new data block or node block. A node block holds up to $(\textit{nodes per block})$ nodes. Data blocks hold the data pointed to by nodes.

\begin{figure}
    \centering
    \includegraphics[width=\columnwidth]{blockpointer.png}
    \caption{Structure of a block pointer.}
    \label{fig:blockpointer}
\end{figure}

\textit{Block pointers} Are used by nodes to point to blocks of storage. Block pointers are 8 bytes long, and are structured as shown in Figure \label{fig:blockpointer}. Bits 5-7 of byte 0 are always 0. If bit 4 of byte 0 is 1, the block pointer points to a \textit{indirection block}, which is a block containing an array of block pointers. A storage-efficient HDPS implementation should use as few indirection blocks as possible.

Bytes 6 and 7 can be used to specify a contiguous sequence of blocks. A sequence of blocks specified by the block count cannot span multiple groups. The number of blocks specified by a block pointer is $1+(block count)$, so a block count value of 0 means that the block pointer is referring to one block.

\begin{figure}
    \centering
    \includegraphics[width=\columnwidth]{nodepointer.png}
    \caption{Structure of a node pointer.}
    \label{fig:nodepointer}
\end{figure}

A local node pointer is 6 bytes in length, and contains the block address of another node. A remote node pointer is 12 bytes long, and can point to a node on a remote machine. In the future, multiple types of remote node pointers may be defined, but the only one currently defined refers to a remote node by the remote system's IP address and block address for that node. Observe that the block address contained by this node has no meaning outside of the remote machine.


\subsection{Block Groups}
\label{ssec:groups}
WRITE THIS LATER (first block of each group has timestamps for checking stuff, and bitmap of what blocks are being used). The block bitmap consists of $(\textit{nodes per block}) \cdot (\textit{blocks per group})$ bits. The first $(\textit{nodes per block})$ bits of the bitmap are always set, since the entirety of the first block is used to hold the block group header. Note that this bitmap requires that the size of the blocks be large enough to contain the bitmap.


\subsection{Nodes}
\label{ssec:nodes}
The information represented by a node is not limited to files; similar to the Unix "everything is a file" paradigm, an operating system using HDPS may represent all machine facilities as nodes. A node is made up of the following components:

\begin{center}
\begin{tabular}{ |c c|l| }
 \hline
 start byte & end byte & contents \\
 \hline
 0 & 7 & Time and date of the node's creation. \\
 8 & 15 & Time and date the node was last modified. \\
 16 & 27 & Node pointer to a node that uses this node as a tag. \\
 32 & 255 & Human-readable name for the node, represented in UTF-8. \\
 256 & 255 + $12 \cdot \textit{(max. tags per node)}$ & Tag table \\
 256 + $12 \cdot \textit{(max. tags per node)}$ & $\frac{block size}{(\textit{nodes per block})} - 1$ & Block table \\
 \hline
\end{tabular}
\end{center}

Most of the fields in a node are self-explanatory. The tag table is a list of pairs of node pointers. The first node pointer in each pair points to a node that this node uses as a tag, and the second node pointer points to another node that uses the same node as a tag. If only a single node uses some other node as a tag, the second pointer in the tag pair will point to the node itself (i.e, if A uses B as a tag, and no other node uses B as a tag, then the node pair in A would contain $(B, A)$). This is equivalent to the $\lambda(v)$ value in the mathematical model. Bytes 16-27 contain a pointer to any node that has this node as a tag. This is equivalent to $c$ in the mathematical model.


\subsection{Suggestions}
\label{ssec:suggest}
Modern computers benefit greatly from high-speed solid state storage. While flash storage provides significantly faster access times than older spinning-disk media, high-capacity flash storage is relatively expensive. Because nodes do not need to be stored on the same media device as their data blocks, HDPS allows the operating system to keep the most frequently-accessed blocks (those containing nodes) on high-speed storage volumes, then keep less-commonly-accessed blocks on slower, more affordable storage. When an operating system detects that heterogeneous storage media is available on the machine, it should attempt to store as many nodes as possible on the highest-speed storage volumes.

For x86, consider using 4096-byte blocks, groups of 16,384 blocks, 1 node per block, and a tag-per-node maximum of 240. Because the first block of each group is reserved, a block pointer can point to only up to 16,383 blocks, giving a single node a maximum capacity of 63.99 MiB. Nodes can contain up to 120 block pointers without using indirection blocks. As such, a node can hold a file up to 7.5 GiB in size without using any indirection blocks. A single indirection block here can hold up to 512 block pointers. A node where all block pointers are indirection blocks can thus hold up to 3.75TiB. The bit map in each group header is 2048 bytes long. The maximum number of block groups per media device


\vfill\null
\newpage
\nocite{*} % Force show all references
\bibliographystyle{ACM-Reference-Format}
\bibliography{bibliography}

\end{document}
