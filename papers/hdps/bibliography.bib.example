% Original CEASAR Paper
@INPROCEEDINGS{ceaser, 
author = {Qureshi, Moinuddin K.},
 title = {CEASER: Mitigating Conflict-based Cache Attacks via Encrypted-address and Remapping},
 booktitle = {Proceedings of the 51st Annual IEEE/ACM International Symposium on Microarchitecture},
 series = {MICRO-51},
 year = {2018},
 isbn = {978-1-5386-6240-3},
 location = {Fukuoka, Japan},
 pages = {775--787},
 numpages = {13},
 url = {https://doi.org/10.1109/MICRO.2018.00068},
 doi = {10.1109/MICRO.2018.00068},
 acmid = {3343332},
 publisher = {IEEE Press},
 address = {Piscataway, NJ, USA},
}

% FRAMER
@article{framer,
  author    = {Anirban Chakraborty,
               Sarani Bhattacharya, and
               Debdeep Mukhopadhyay},
  title     = {Using Memory Allocation Schemes in Linux to Exploit {DRAM} Vulnerability:
               with Rowhammer as a Case Study},
  journal   = {CoRR},
  volume    = {abs/1905.12974},
  year      = {2019},
  url       = {http://arxiv.org/abs/1905.12974},
  archivePrefix = {arXiv},
  eprint    = {1905.12974},
  timestamp = {Mon, 03 Jun 2019 13:42:33 +0200},
  biburl    = {https://dblp.org/rec/bib/journals/corr/abs-1905-12974},
  bibsource = {dblp computer science bibliography, https://dblp.org}
}

% perf
@inproceedings{perf,
  author    = {M.M. Michael and A.K. Nanda},
  title     = {Design and performance of directory caches for scalable shared memory multiprocessors},
  booktitle = {Proceedings Fifth International Symposium on High--Performance Computer Architecture},
  series = {HPCA 1999},
  year = {1999},
  isbn = {0-7695-0004-8},
  location = {Orlando, FL, USA},
  pages = {142-151},
  numpages = {16},
  url = {https://doi.org/10.1109/SP.2019.00004},
  acmid = {000000},
  publisher = {Institute of Electrical and Electronics Engineers},
  address = {San Francisco, CA, United States},
}

% Attack Directories, Not Caches
@inproceedings{attack,
  author    = {Mengjia Yan,
               Read Sprabery,
               Bhargava Gopireddy,
               Christopher Fletcher,
               Roy Campbell, and
               Josep Torrellas},
  title     = {Attack Directories, Not Caches: Side-Channel Attacks in a Non-Inclusive World},
  booktitle = {Proceedings of the 40th IEEE Symposium on Security \& Privacy},
  series = {SP 2019},
  year = {2019},
  isbn = {978-1-538666-60-9},
  location = {Oakland, CA, USA},
  pages = {888--904},
  numpages = {16},
  url = {https://doi.org/10.1109/SP.2019.00004},
  acmid = {8835325},
  publisher = {Institute of Electrical and Electronics Engineers},
  address = {San Francisco, CA, United States},
}

% ARMageddon
@inproceedings{armageddon,
 author = {Lipp, Moritz and Gruss, Daniel and Spreitzer, Raphael and Maurice, Cl{\'e}mentine and Mangard, Stefan},
 title = {ARMageddon: Cache Attacks on Mobile Devices},
 booktitle = {Proceedings of the 25th USENIX Conference on Security Symposium},
 series = {SEC'16},
 year = {2016},
 isbn = {978-1-931971-32-4},
 location = {Austin, TX, USA},
 pages = {549--564},
 numpages = {16},
 url = {http://dl.acm.org/citation.cfm?id=3241094.3241138},
 acmid = {3241138},
 publisher = {USENIX Association},
 address = {Berkeley, CA, USA},
} 

% Cache Games
@inproceedings{cachegames,
 author = {Gullasch, David and Bangerter, Endre and Krenn, Stephan},
 title = {Cache Games -- Bringing Access-Based Cache Attacks on AES to Practice},
 booktitle = {Proceedings of the 2011 IEEE Symposium on Security and Privacy},
 series = {SP '11},
 year = {2011},
 isbn = {978-0-7695-4402-1},
 pages = {490--505},
 numpages = {16},
 url = {https://doi.org/10.1109/SP.2011.22},
 doi = {10.1109/SP.2011.22},
 acmid = {2006784},
 publisher = {IEEE Computer Society},
 address = {Washington, DC, USA},
 keywords = {AES, access-based cache attacks, side channel},
}

% SecDir
@inproceedings{secdir,
 author = {Yan, Mengjia and Wen, Jen-Yang and Fletcher, Christopher W. and Torrellas, Josep},
 title = {SecDir: A Secure Directory to Defeat Directory Side-channel Attacks},
 booktitle = {Proceedings of the 46th International Symposium on Computer Architecture},
 series = {ISCA '19},
 year = {2019},
 isbn = {978-1-4503-6669-4},
 location = {Phoenix, Arizona},
 pages = {332--345},
 numpages = {14},
 url = {http://doi.acm.org/10.1145/3307650.3326635},
 doi = {10.1145/3307650.3326635},
 acmid = {3326635},
 publisher = {ACM},
 address = {New York, NY, USA},
 keywords = {cache-coherence directories, cuckoo hashing, side-channel attacks},
}

%
@inproceedings{scas,
  TITLE = {{Improving Confidentiality Against Cache-based SCAs}},
  AUTHOR = {Mushtaq, Maria and Lapotre, Vianney and Gogniat, Guy and Mukhtar, M Asim and Khurram Bhatti, Muhammad},
  URL = {https://hal.archives-ouvertes.fr/hal-01748057},
  BOOKTITLE = {{ACM WomENcourage}},
  ADDRESS = {barcelona, France},
  YEAR = {2017},
  MONTH = Sep,
  KEYWORDS = {Countermeasure ; RSA ; Side-Channel Attacks ; Caches ; Cryptography},
  PDF = {https://hal.archives-ouvertes.fr/hal-01748057/file/womENcourage_2017_paper_10.pdf},
  HAL_ID = {hal-01748057},
  HAL_VERSION = {v1},
}


% GEM5
@article{gem5,
 author = {Binkert, Nathan and Beckmann, Bradford and Black, Gabriel and Reinhardt, Steven K. and Saidi, Ali and Basu, Arkaprava and Hestness, Joel and Hower, Derek R. and Krishna, Tushar and Sardashti, Somayeh and Sen, Rathijit and Sewell, Korey and Shoaib, Muhammad and Vaish, Nilay and Hill, Mark D. and Wood, David A.},
 title = {The Gem5 Simulator},
 journal = {SIGARCH Comput. Archit. News},
 issue_date = {May 2011},
 volume = {39},
 number = {2},
 month = aug,
 year = {2011},
 issn = {0163-5964},
 pages = {1--7},
 numpages = {7},
 url = {http://doi.acm.org/10.1145/2024716.2024718},
 doi = {10.1145/2024716.2024718},
 acmid = {2024718},
 publisher = {ACM},
 address = {New York, NY, USA},
} 
